﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ORMS.Models
{
    public class SizeBreakdown
    {
        [Key]
        public int Id { get; set; }

        [Required(ErrorMessage ="Enter color")]
        [ForeignKey("ColorRange")]
        public int ColorRangeId { get; set; }

        [Required(ErrorMessage = "Enter size")]
        [ForeignKey("SizeRange")]
        public int SizeRangeId { get; set; }

        public double UnitPrice { get; set; }

        public double Quantity { get; set; }

        public double TotalCost { get; set; }

        [Required(ErrorMessage = "Enter order recap")]
        [ForeignKey("OrderRecap")]
        public int OrderRecapId { get; set; }

        public virtual ColorRange ColorRange { get; set; }
        public virtual SizeRange SizeRange { get; set; }
        public virtual OrderRecap OrderRecap { get; set; }
    }
}