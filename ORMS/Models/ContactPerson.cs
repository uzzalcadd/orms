﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ORMS.Models
{
    public class ContactPerson
    {
        [Key]
        public int Id { get; set; }

        [Required(ErrorMessage ="Enter Name")]
        [StringLength(50)]
        public string Name { get; set; }

        [StringLength(300)]
        public string Address { get; set; }

        [Required(ErrorMessage ="Enter Phone Number")]
        [StringLength(50)]
        public string Phone { get; set; }

        [Required(ErrorMessage ="Enter Email")]
        [DataType(DataType.EmailAddress)]
        [StringLength(100)]
        public string Email { get; set; }

        [StringLength(100)]
        public string Note { get; set; }
    }
}