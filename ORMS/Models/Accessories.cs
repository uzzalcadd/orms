﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ORMS.Models
{
    public class Accessories
    {
        [Key]
        public int Id { get; set; }

        [Required(ErrorMessage ="Enter Code")]
        [Index(IsUnique = true)]
        [StringLength(20)]
        public string Code { get; set; }

        [ForeignKey("Product")]
        public int ProductId { get; set; }

        [Required(ErrorMessage ="Enter Sequence")]
        [StringLength(100)]
        public string Sequence { get; set; }

        public string Unit { get; set; }

        public double ConDOz { get; set; }

        public double Price { get; set; }

        public double CostDoz { get; set; }

        public double CostPc { get; set; }

        public double TotalCost { get; set; }

        [ForeignKey("PreCost")]
        public int PreCostId { get; set; }

        public virtual PreCostSheet PreCost { get; set; }
        public virtual Product Product { get; set; }
    }
}