﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ORMS.Models
{
    public class OrderRecap
    {
        [Key]
        public int Id { get; set; }

        [Required(ErrorMessage = "Style cannot be empty")]
        [ForeignKey("Style")]
        public int StyleId { get; set; }

        [Required(ErrorMessage = "Order Number cannot be empty")]
        public int OrderNo { get; set; }

        public string Session { get; set; }

        public int Sl { get; set; }

        [Required(ErrorMessage ="Select Product")]
        [ForeignKey("Product")]
        public int ProductId { get; set; }

        [Required(ErrorMessage ="Select Buyer")]
        [ForeignKey("Buyer")]
        public int BuyerId { get; set; }

        [Required(ErrorMessage ="Select Contact Person")]
        [ForeignKey("ContactPerson")]
        public int ContactPersonId { get; set; }

        [Required(ErrorMessage = "Count cannot be empty")]
        public int Count { get; set; }

        [Required(ErrorMessage = "GSM cannot be empty")]
        public string GSM { get; set; }

        [DataType(DataType.Date),
        DisplayFormat(DataFormatString = "{0:dd-MMM-yyyy}",
        ApplyFormatInEditMode = true)]
        public DateTime OrderDate { get; set; }

        [DataType(DataType.Date),
        DisplayFormat(DataFormatString = "{0:dd-MMM-yyyy}",
        ApplyFormatInEditMode = true)]
        public DateTime ShipmetDate { get; set; }

        [DataType(DataType.Date),
        DisplayFormat(DataFormatString = "{0:dd-MMM-yyyy}",
        ApplyFormatInEditMode = true)]
        public DateTime FatoryShipmentDate { get; set; }

        [Required(ErrorMessage = "Transport type required!")]
        public string TransportType { get; set; }

        [Required(ErrorMessage ="Select a Marchendiser")]
        [ForeignKey("Marchendiser")]
        public int MarchendiserId { get; set; }

        [Required(ErrorMessage ="Select Currency")]
        [StringLength(30)]
        public string Currency { get; set; }

        [Required(ErrorMessage ="Production Factory cannot be empty")]
        [ForeignKey("ProductionFactory")]
        public int ProductionFactoryId { get; set; }

        [Required(ErrorMessage ="Enter Delivery term")]
        [StringLength(50)]
        public string DeliveryTerm { get; set; }

        [Required(ErrorMessage ="Enter Incoterm")]
        [StringLength(100)]
        public string Incoterm { get; set; }

        [Required(ErrorMessage ="Enter fabrication")]
        [StringLength(200)]
        public string Fabrcation { get; set; }

        [StringLength(300)]
        public string Remarks { get; set; }

        public virtual Style Style { get; set; }
        public virtual Product Product { get; set; }
        public virtual Buyer Buyer { get; set; }
        public virtual ContactPerson ContactPerson { get; set; }
        public virtual Marchendiser Marchendiser { get; set; }
        public virtual ProducttionFactory ProductionFactory { get; set; }
    }
    }