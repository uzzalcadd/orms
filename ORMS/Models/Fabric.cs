﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ORMS.Models
{
    public class Fabric
    {
        [Key]
        public int Id { get; set; }

        [Required(ErrorMessage ="Name cannot be empty")]
        [StringLength(100)]
        public string Name { get; set; }

        [StringLength(200)]
        public string Description { get; set; }
    }
}