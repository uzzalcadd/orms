﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ORMS.Models
{
    public class Total
    {
        [Key]
        public int Id { get; set; }

        public double TotalFabricCost { get; set; }

        public double TotalAccesoriesCost { get; set; }

        public double OtherCost { get; set; }

        public double FOBCost { get; set; }

        [ForeignKey("PreCost")]
        public int PreCostId { get; set; }

        public virtual PreCostSheet PreCost { get; set; }
    }
}