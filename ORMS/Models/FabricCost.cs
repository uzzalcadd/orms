﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ORMS.Models
{
    public class FabricCost
    {
        [Key]
        public int Id { get; set; }

        [ForeignKey("Fabric")]
        public int FabricId { get; set; }

        [Required(ErrorMessage = "Enter Code")]
        [Index(IsUnique = true)]
        [StringLength(20)]
        public string Code { get; set; }

        [Required(ErrorMessage = "Enter Sequence")]
        [StringLength(100)]
        public string Sequence { get; set; }

        public string Unit { get; set; }

        public double Yarn { get; set; }

        public double Knitting { get; set; }

        public double Dying { get; set; }

        public double Lycra { get; set; }

        public double AOP { get; set; }

        public double Wash { get; set; }

        public double YD { get; set; }

        public double Finish { get; set; }

        public double Brash { get; set; }

        public double Fabriccost { get; set; }

        public double ConDoz { get; set; }

        public double ConPc { get; set; }

        public double ValPc { get; set; }

        public double ValDoz { get; set; }

        [ForeignKey("PreCost")]
        public int PreCostId { get; set; }

        public virtual PreCostSheet PreCost { get; set; }
        public virtual Fabric Fabric { get; set; }
    }
}