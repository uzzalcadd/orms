﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ORMS.Models
{
    public class ProducttionFactory
    {
        [Key]
        public int Id { get; set; }

        [Required(ErrorMessage ="Name cannot be empty")]
        [StringLength(100)]
        public string Name { get; set; }

        [StringLength(300)]
        public string Address { get; set; }

        [Required(ErrorMessage ="Email cannot be empty")]
        [StringLength(100)]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [StringLength(50)]
        public string ShortName { get; set; }
    }
}