﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ORMS.Models
{
    public class PreCostSheet
    {
        [Key]
        public int Id { get; set; }

        [Required(ErrorMessage = "Select Style")]
        [ForeignKey("Style")]
        public int StyleId { get; set; }

        [Required(ErrorMessage = "Enter file number")]
        public int FileNumber { get; set; }

        [DataType(DataType.Date),
        DisplayFormat(DataFormatString = "{0:dd-MMM-yyyy}",
        ApplyFormatInEditMode = true)]
        public DateTime CostingDate { get; set; }

        [Required(ErrorMessage = "Select Buyer")]
        [ForeignKey("Buyer")]
        public int BuyerId { get; set; }

        [Required(ErrorMessage = "Enter quantity")]
        public double Quantity { get; set; }

        [Required(ErrorMessage = "Knitting type cannot be empty")]
        [ForeignKey("KnittingType")]
        public int KnittingTypeId { get; set; }

        [Required(ErrorMessage = "Enter GSM")]
        public string GSM { get; set; }

        [Required(ErrorMessage = "Enter Color range")]
        public string ColorRange { get; set; }

        [Required(ErrorMessage = "Enter Size range")]
        public string SizeRange { get; set; }

        [StringLength(300)]
        public string StyleDescription { get; set; }

        [Required(ErrorMessage = "Select Marchendiser")]
        [ForeignKey("Marchendiser")]
        public int MarchendiserId { get; set; }

        [StringLength(200)]
        public string FabricDescription { get; set; }

        [Required(ErrorMessage = "Enter cost currency")]
        [StringLength(50)]
        public string CostCurrency { get; set; }

        public virtual Style Style { get; set; }
        public virtual Buyer Buyer { get; set; }
        public virtual Marchendiser Marchendiser { get; set; }
        public virtual KnittingType KnittingType { get; set; }
    }
}