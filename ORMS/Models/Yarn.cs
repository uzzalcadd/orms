﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ORMS.Models
{
    public class Yarn
    {
        [Key]
        public int Id { get; set; }

        [Required(ErrorMessage ="Enter a Code")]
        [Index(IsUnique = true)]
        [StringLength(20)]
        public string Code { get; set; }

        [Required(ErrorMessage ="Enter Name")]
        [StringLength(100)]
        public string Name { get; set; }

        [Required(ErrorMessage ="Enter Sequence")]
        [StringLength(100)]
        public string Sequence { get; set; }

        [Required(ErrorMessage ="Enter ply")]
        [StringLength(20)]
        public string Ply { get; set; }

        public double Price { get; set; }

        public double Use { get; set; }

        public double EstimatedCost { get; set; }

        public double Wastage { get; set; }

    }
}