﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ORMS.Models
{
    public class Marchendiser
    {
        [Key]
        public int Id { get; set; }

        [Required(ErrorMessage ="Enter Name")]
        [StringLength(100)]
        public string Name { get; set; }

        [StringLength(300)]
        public string Address { get; set; }

        [Required(ErrorMessage ="Enter Mobile Number")]
        [StringLength(50)]
        public string Mobile { get; set; }

        [Required(ErrorMessage ="Enter Email")]
        [StringLength(100)]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
    }
}