﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ORMS.Models
{
    public class Style
    {
        [Key]
        public int Id { get; set; }

        [Required(ErrorMessage = "Style No cannot be empty")]
        public string StyleNo { get; set; }

        [Required(ErrorMessage ="Knit cannot be empty")]
        public string Knit { get; set; }

        [Required(ErrorMessage ="Session cannot be empty")]
        [StringLength(100)]
        public string Session { get; set; }

        [Required(ErrorMessage ="Bayer Cannot be empty")]
        [ForeignKey("Buyer")]
        public int BuyerId { get; set; }

        [Required(ErrorMessage ="Contact Person cannot be empty")]
        [ForeignKey("ContactPerson")]
        public int ContactPersonId { get; set; }

        [ForeignKey("Brand")]
        public int? BrandId { get; set; }

        [Required(ErrorMessage ="Department cannot be empty")]
        [ForeignKey("Department")]
        public int DepartmentId { get; set; }

        [Required(ErrorMessage ="Knitting type cannot be empty")]
        [ForeignKey("KnittingType")]
        public int KnittingTypeId { get; set; }

        [Required(ErrorMessage = "GSM cannot be empty")]
        [StringLength(50)]
        public string GSM { get; set; }

        [Required(ErrorMessage = "Fabric cannot be empty")]
        [ForeignKey("Fabric")]
        public int FabricId { get; set; }

        [Required(ErrorMessage = "Count cannot be empty")]
        public int Count { get; set; }

        [Required(ErrorMessage = "Color Range cannot be empty")]
        public string ColorRange { get; set; }

        [Required(ErrorMessage = "Color Range cannot be empty")]
        public string SizeRange { get; set; }

        [Required(ErrorMessage = "Color Range cannot be empty")]
        [StringLength(200)]
        public string Fabrication { get; set; }

        public string StyleDescription { get; set; }

        [Required(ErrorMessage = "Marchendiser cannot be empty")]
        [ForeignKey("Marchendiser")]
        public int MerchandiserId { get; set; }

        public byte[] DesignSketch { get; set; }

        public virtual Buyer Buyer { get; set; }
        public virtual ContactPerson ContactPerson { get; set; }
        public virtual Brand Brand { get; set; }
        public virtual Department Department { get; set; }
        public virtual KnittingType KnittingType { get; set; }
        public virtual Fabric Fabric { get; set; }
        public virtual Marchendiser Marchendiser { get; set; }
    }
}