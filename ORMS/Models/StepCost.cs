﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ORMS.Models
{
    public class StepCost
    {
        [Key]
        public int Id { get; set; }

        public string StepName { get; set; }
        
        public string Remarks { get; set; } 

        public double CostPc { get; set; }

        public double CostDoz { get; set; }

        [ForeignKey("PreCost")]
        public int PreCostId { get; set; }

        public virtual PreCostSheet PreCost { get; set; }
    }
}