﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace ORMS.Models
{
    public class ORMSDbContext : DbContext
    {
        public ORMSDbContext():base("ORMSDbContext")
        {
            Database.SetInitializer<ORMSDbContext>(null);
            Configuration.ProxyCreationEnabled = true;
            Configuration.LazyLoadingEnabled = true;
        }

        public DbSet<Accessories> Accessories { get; set; }
        public DbSet<User> User { get; set; }
        public DbSet<Brand> Brand { get; set; }
        public DbSet<Buyer> Buyer { get; set; }
        public DbSet<ColorRange> ColorRange { get; set; }
        public DbSet<ContactPerson> ContactPerson { get; set; }
        public DbSet<Department> Department { get; set; }
        public DbSet<Fabric> Fabric { get; set; }
        public DbSet<KnittingType> KnittingType { get; set; }
        public DbSet<Marchendiser> Marchendiser { get; set; }
        public DbSet<OrderRecap> OrderRecap { get; set; }
        public DbSet<PreCostSheet> PreCostSheet { get; set; }
        public DbSet<Product> Product { get; set; }
        public DbSet<ProducttionFactory> ProducttionFactory { get; set; }
        public DbSet<RefType> RefType { get; set; }
        public DbSet<SizeBreakdown> SizeBreakdown { get; set; }
        public DbSet<SizeRange> SizeRange { get; set; }
        public DbSet<Style> Style { get; set; }
        public DbSet<Yarn> Yarn { get; set; }
        public DbSet<FabricCost> FabricCost { get; set; }
        public DbSet<StepCost> StepCost { get; set; }
        public DbSet<Total> Total { get; set; }
    }
}