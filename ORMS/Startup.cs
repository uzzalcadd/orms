﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ORMS.Startup))]
namespace ORMS
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
