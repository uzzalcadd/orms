﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ORMS.Models;

namespace ORMS.Controllers
{
    public class RefTypesController : Controller
    {
        private ORMSDbContext db = new ORMSDbContext();

        // GET: RefTypes
        public ActionResult Index()
        {
            return View(db.RefType.ToList());
        }

        // GET: RefTypes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RefType refType = db.RefType.Find(id);
            if (refType == null)
            {
                return HttpNotFound();
            }
            return View(refType);
        }

        // GET: RefTypes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: RefTypes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name")] RefType refType)
        {
            if (ModelState.IsValid)
            {
                db.RefType.Add(refType);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(refType);
        }

        // GET: RefTypes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RefType refType = db.RefType.Find(id);
            if (refType == null)
            {
                return HttpNotFound();
            }
            return View(refType);
        }

        // POST: RefTypes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name")] RefType refType)
        {
            if (ModelState.IsValid)
            {
                db.Entry(refType).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(refType);
        }

        // GET: RefTypes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RefType refType = db.RefType.Find(id);
            if (refType == null)
            {
                return HttpNotFound();
            }
            return View(refType);
        }

        // POST: RefTypes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            RefType refType = db.RefType.Find(id);
            db.RefType.Remove(refType);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
