﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ORMS.Models;

namespace ORMS.Controllers
{
    public class FabricCostsController : Controller
    {
        private ORMSDbContext db = new ORMSDbContext();

        // GET: FabricCosts
        public ActionResult Index(int id)
        {
            var fabricCost = db.FabricCost.Include(f => f.PreCost).Where(i => i.PreCostId == id);
            ViewBag.CostId = id;
            return View(fabricCost.ToList());
        }

        // GET: FabricCosts/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FabricCost fabricCost = db.FabricCost.Find(id);
            if (fabricCost == null)
            {
                return HttpNotFound();
            }
            return View(fabricCost);
        }

        // GET: FabricCosts/Create
        public ActionResult Create()
        {
            ViewBag.PreCostId = new SelectList(db.PreCostSheet, "Id", "GSM");
            return View();
        }

        // POST: FabricCosts/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Code,Sequence,Unit,Yarn,Knitting,Dying,Lycra,AOP,Wash,YD,Finish,Brash,Fabriccost,ConDoz,ConPc,ValPc,ValDoz,PreCostId")] FabricCost fabricCost)
        {
            if (ModelState.IsValid)
            {
                db.FabricCost.Add(fabricCost);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.PreCostId = new SelectList(db.PreCostSheet, "Id", "GSM", fabricCost.PreCostId);
            return View(fabricCost);
        }

        // GET: FabricCosts/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FabricCost fabricCost = db.FabricCost.Find(id);
            if (fabricCost == null)
            {
                return HttpNotFound();
            }
            ViewBag.PreCostId = new SelectList(db.PreCostSheet, "Id", "GSM", fabricCost.PreCostId);
            return View(fabricCost);
        }

        // POST: FabricCosts/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Code,Sequence,Unit,Yarn,Knitting,Dying,Lycra,AOP,Wash,YD,Finish,Brash,Fabriccost,ConDoz,ConPc,ValPc,ValDoz,PreCostId")] FabricCost fabricCost)
        {
            if (ModelState.IsValid)
            {
                db.Entry(fabricCost).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.PreCostId = new SelectList(db.PreCostSheet, "Id", "GSM", fabricCost.PreCostId);
            return View(fabricCost);
        }

        // GET: FabricCosts/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FabricCost fabricCost = db.FabricCost.Find(id);
            if (fabricCost == null)
            {
                return HttpNotFound();
            }
            return View(fabricCost);
        }

        // POST: FabricCosts/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            FabricCost fabricCost = db.FabricCost.Find(id);
            db.FabricCost.Remove(fabricCost);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
