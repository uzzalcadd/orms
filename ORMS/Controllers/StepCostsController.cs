﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ORMS.Models;

namespace ORMS.Controllers
{
    public class StepCostsController : Controller
    {
        private ORMSDbContext db = new ORMSDbContext();

        // GET: StepCosts
        public ActionResult Index( int id)
        {
            var stepCost = db.StepCost.Include(s => s.PreCost).Where(i => i.PreCostId == id);
            ViewBag.CostId = id;
            return View(stepCost.ToList());
        }

        // GET: StepCosts/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            StepCost stepCost = db.StepCost.Find(id);
            if (stepCost == null)
            {
                return HttpNotFound();
            }
            return View(stepCost);
        }

        // GET: StepCosts/Create
        public ActionResult Create()
        {
            ViewBag.PreCostId = new SelectList(db.PreCostSheet, "Id", "GSM");
            return View();
        }

        // POST: StepCosts/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,StepName,Remarks,CostPc,CostDoz,PreCostId")] StepCost stepCost)
        {
            if (ModelState.IsValid)
            {
                db.StepCost.Add(stepCost);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.PreCostId = new SelectList(db.PreCostSheet, "Id", "GSM", stepCost.PreCostId);
            return View(stepCost);
        }

        // GET: StepCosts/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            StepCost stepCost = db.StepCost.Find(id);
            if (stepCost == null)
            {
                return HttpNotFound();
            }
            ViewBag.PreCostId = new SelectList(db.PreCostSheet, "Id", "GSM", stepCost.PreCostId);
            return View(stepCost);
        }

        // POST: StepCosts/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,StepName,Remarks,CostPc,CostDoz,PreCostId")] StepCost stepCost)
        {
            if (ModelState.IsValid)
            {
                db.Entry(stepCost).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.PreCostId = new SelectList(db.PreCostSheet, "Id", "GSM", stepCost.PreCostId);
            return View(stepCost);
        }

        // GET: StepCosts/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            StepCost stepCost = db.StepCost.Find(id);
            if (stepCost == null)
            {
                return HttpNotFound();
            }
            return View(stepCost);
        }

        // POST: StepCosts/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            StepCost stepCost = db.StepCost.Find(id);
            db.StepCost.Remove(stepCost);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
