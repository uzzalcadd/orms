﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ORMS.Models;

namespace ORMS.Controllers
{
    public class TotalsController : Controller
    {
        private ORMSDbContext db = new ORMSDbContext();

        // GET: Totals
        public ActionResult Index(int id)
        {
            var total = db.Total.Include(t => t.PreCost).Where( i => i.PreCostId == id);
            ViewBag.CostId = id;
            return View(total.ToList());
        }

        // GET: Totals/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Total total = db.Total.Find(id);
            if (total == null)
            {
                return HttpNotFound();
            }
            return View(total);
        }

        // GET: Totals/Create
        public ActionResult Create()
        {
            ViewBag.PreCostId = new SelectList(db.PreCostSheet, "Id", "GSM");
            return View();
        }

        // POST: Totals/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,TotalFabricCost,TotalAccesoriesCost,OtherCost,FOBCost,PreCostId")] Total total)
        {
            if (ModelState.IsValid)
            {
                db.Total.Add(total);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.PreCostId = new SelectList(db.PreCostSheet, "Id", "GSM", total.PreCostId);
            return View(total);
        }

        // GET: Totals/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Total total = db.Total.Find(id);
            if (total == null)
            {
                return HttpNotFound();
            }
            ViewBag.PreCostId = new SelectList(db.PreCostSheet, "Id", "GSM", total.PreCostId);
            return View(total);
        }

        // POST: Totals/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,TotalFabricCost,TotalAccesoriesCost,OtherCost,FOBCost,PreCostId")] Total total)
        {
            if (ModelState.IsValid)
            {
                db.Entry(total).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.PreCostId = new SelectList(db.PreCostSheet, "Id", "GSM", total.PreCostId);
            return View(total);
        }

        // GET: Totals/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Total total = db.Total.Find(id);
            if (total == null)
            {
                return HttpNotFound();
            }
            return View(total);
        }

        // POST: Totals/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Total total = db.Total.Find(id);
            db.Total.Remove(total);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
