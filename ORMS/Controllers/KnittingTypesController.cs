﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ORMS.Models;

namespace ORMS.Controllers
{
    public class KnittingTypesController : Controller
    {
        private ORMSDbContext db = new ORMSDbContext();

        // GET: KnittingTypes
        public ActionResult Index()
        {
            return View(db.KnittingType.ToList());
        }

        // GET: KnittingTypes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            KnittingType knittingType = db.KnittingType.Find(id);
            if (knittingType == null)
            {
                return HttpNotFound();
            }
            return View(knittingType);
        }

        // GET: KnittingTypes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: KnittingTypes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name")] KnittingType knittingType)
        {
            if (ModelState.IsValid)
            {
                db.KnittingType.Add(knittingType);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(knittingType);
        }

        // GET: KnittingTypes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            KnittingType knittingType = db.KnittingType.Find(id);
            if (knittingType == null)
            {
                return HttpNotFound();
            }
            return View(knittingType);
        }

        // POST: KnittingTypes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name")] KnittingType knittingType)
        {
            if (ModelState.IsValid)
            {
                db.Entry(knittingType).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(knittingType);
        }

        // GET: KnittingTypes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            KnittingType knittingType = db.KnittingType.Find(id);
            if (knittingType == null)
            {
                return HttpNotFound();
            }
            return View(knittingType);
        }

        // POST: KnittingTypes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            KnittingType knittingType = db.KnittingType.Find(id);
            db.KnittingType.Remove(knittingType);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
