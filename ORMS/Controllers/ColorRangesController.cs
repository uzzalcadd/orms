﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ORMS.Models;

namespace ORMS.Controllers
{
    public class ColorRangesController : Controller
    {
        private ORMSDbContext db = new ORMSDbContext();

        #region Get Color Range Information
        public ActionResult GetExistingColorRanges()
        {
            string ColorRange = Request["ColorRange"];
            List<ColorRange> ColorRangeList = new List<ColorRange>();
            if(String.IsNullOrWhiteSpace(ColorRange))
            {
                ColorRangeList = db.ColorRange.ToList();
            }
            else
            {
                ColorRangeList = db.ColorRange.Where(i => i.Name.Contains(ColorRange)).ToList();
            }
            return Json(ColorRangeList, JsonRequestBehavior.AllowGet);
        }
        #endregion

        // GET: ColorRanges
        public ActionResult Index()
        {
            return View(db.ColorRange.ToList());
        }

        // GET: ColorRanges/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ColorRange colorRange = db.ColorRange.Find(id);
            if (colorRange == null)
            {
                return HttpNotFound();
            }
            return View(colorRange);
        }

        // GET: ColorRanges/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: ColorRanges/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name,Note")] ColorRange colorRange)
        {
            if (ModelState.IsValid)
            {
                db.ColorRange.Add(colorRange);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(colorRange);
        }

        // GET: ColorRanges/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ColorRange colorRange = db.ColorRange.Find(id);
            if (colorRange == null)
            {
                return HttpNotFound();
            }
            return View(colorRange);
        }

        // POST: ColorRanges/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name,Note")] ColorRange colorRange)
        {
            if (ModelState.IsValid)
            {
                db.Entry(colorRange).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(colorRange);
        }

        // GET: ColorRanges/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ColorRange colorRange = db.ColorRange.Find(id);
            if (colorRange == null)
            {
                return HttpNotFound();
            }
            return View(colorRange);
        }

        // POST: ColorRanges/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ColorRange colorRange = db.ColorRange.Find(id);
            db.ColorRange.Remove(colorRange);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
