﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ORMS.Models;

namespace ORMS.Controllers
{
    public class SizeRangesController : Controller
    {
        private ORMSDbContext db = new ORMSDbContext();

        #region Get Size Range Information
        public ActionResult GetExistingSizeRanges()
        {
            string SizeRange = Request["SizeRange"];
            List<SizeRange> SizeRangeList = new List<SizeRange>();
            if(String.IsNullOrWhiteSpace(SizeRange))
            {
                SizeRangeList = db.SizeRange.ToList();
            }
            else
            {
                SizeRangeList = db.SizeRange.Where(i => i.Name.Contains(SizeRange)).ToList();
            }            
            return Json(SizeRangeList, JsonRequestBehavior.AllowGet);
        }
        #endregion

        // GET: SizeRanges
        public ActionResult Index()
        {
            return View(db.SizeRange.ToList());
        }

        // GET: SizeRanges/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SizeRange sizeRange = db.SizeRange.Find(id);
            if (sizeRange == null)
            {
                return HttpNotFound();
            }
            return View(sizeRange);
        }

        // GET: SizeRanges/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: SizeRanges/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name,Note")] SizeRange sizeRange)
        {
            if (ModelState.IsValid)
            {
                db.SizeRange.Add(sizeRange);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(sizeRange);
        }

        // GET: SizeRanges/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SizeRange sizeRange = db.SizeRange.Find(id);
            if (sizeRange == null)
            {
                return HttpNotFound();
            }
            return View(sizeRange);
        }

        // POST: SizeRanges/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name,Note")] SizeRange sizeRange)
        {
            if (ModelState.IsValid)
            {
                db.Entry(sizeRange).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(sizeRange);
        }

        // GET: SizeRanges/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SizeRange sizeRange = db.SizeRange.Find(id);
            if (sizeRange == null)
            {
                return HttpNotFound();
            }
            return View(sizeRange);
        }

        // POST: SizeRanges/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            SizeRange sizeRange = db.SizeRange.Find(id);
            db.SizeRange.Remove(sizeRange);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
