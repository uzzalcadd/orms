﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ORMS.Models;

namespace ORMS.Controllers
{
    public class PreCostSheetsController : Controller
    {
        private ORMSDbContext db = new ORMSDbContext();

        // GET: PreCostSheets
        public ActionResult Index()
        {
            var preCostSheet = db.PreCostSheet.Include(p => p.Buyer).Include(p => p.KnittingType).Include(p => p.Marchendiser).Include(p => p.Style);
            return View(preCostSheet.ToList());
        }

        // GET: PreCostSheets/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PreCostSheet preCostSheet = db.PreCostSheet.Find(id);
            if (preCostSheet == null)
            {
                return HttpNotFound();
            }
            return View(preCostSheet);
        }


        #region GetSelectedAccesories
        [AllowAnonymous]
        [HttpPost]
        public ActionResult GetSelectedAccesories()
        {
            if (Request["ProductId"].ToString() != "")
            {
                int ProductId = Convert.ToInt32(Request["ProductId"]);
                string Code = Request["Code"];
                string Sequence = Request["Sequence"];
                string Unit = Request["Unit"];
                double ConDoz = Convert.ToDouble(Request["ConDoz"]);
                double Price = Convert.ToDouble(Request["Price"]);
                double CostDoz = Convert.ToDouble(Request["CostDoz"]);
                double CostPc = Convert.ToDouble(Request["CostPc"]);
                double TotalCost = Convert.ToDouble(Request["TotalCost"]);

                List<Accessories> ItemList = new List<Accessories>();
                ItemList = (List<Accessories>)Session["ItemList1"];
                if (ItemList.Count() > 0)
                {
                    Accessories details = new Accessories();
                    details.Id = ItemList.Count() + 1;
                    details.ProductId = ProductId;
                    details.Code = Code;
                    details.Sequence = Sequence;
                    details.Unit = Unit;
                    details.ConDOz = ConDoz;
                    details.Price = Price;
                    details.CostDoz = CostDoz;
                    details.CostPc = CostPc;
                    details.TotalCost = TotalCost;
                    details.Product = db.Product.Find(ProductId);
                    ItemList.Add(details);
                    Session["ItemList1"] = ItemList;
                }
                else
                {
                    List<Accessories> ItemLst = new List<Accessories>();
                    Accessories details = new Accessories();
                    details.Id = 1;
                    details.ProductId = ProductId;
                    details.Code = Code;
                    details.Sequence = Sequence;
                    details.Unit = Unit;
                    details.ConDOz = ConDoz;
                    details.Price = Price;
                    details.CostDoz = CostDoz;
                    details.CostPc = CostPc;
                    details.TotalCost = TotalCost;
                    details.Product = db.Product.Find(ProductId);
                    ItemLst.Add(details);
                    Session["ItemList1"] = ItemLst;
                }
            }
            return Json(Session["ItemList1"], JsonRequestBehavior.AllowGet);
        }

        [AllowAnonymous]
        [HttpPost]
        public ActionResult DeleteFromList()
        {
            int DetailsId = Convert.ToInt32(Request["DetailsId"]);
            List<Accessories> ItemLst = new List<Accessories>();
            ItemLst = (List<Accessories>)Session["ItemList1"];
            for (int i = 0; i < ItemLst.Count; i++)
            {
                if (ItemLst[i].Id == DetailsId)
                {
                    ItemLst.Remove(ItemLst[i]);
                }
            }
            Session["ItemList1"] = null;

            if (ItemLst.Count > 0)
            {
                Session["ItemList1"] = ItemLst;
            }
            else
            {
                Session["ItemList1"] = new List<Accessories>();
            }

            return Json(Session["ItemList1"], JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region GetSelectedFabricCost
        [AllowAnonymous]
        [HttpPost]
        public ActionResult GetSelectedFabricCost()
        {
            if (Request["FabricId"].ToString() != "")
            {
                int FabricId = Convert.ToInt32(Request["FabricId"]);
                string Code = Request["Code"];
                string Sequence = Request["Sequence"];
                string Unit = Request["Unit"];
                double Yarn = Convert.ToDouble(Request["Yarn"]);
                double Knitting = Convert.ToDouble(Request["Knitting"]);
                double Dying = Convert.ToDouble(Request["Dying"]);
                double Lycra = Convert.ToDouble(Request["Lycra"]);
                double AOP = Convert.ToDouble(Request["AOP"]);
                double Wash = Convert.ToDouble(Request["Wash"]);
                double YD = Convert.ToDouble(Request["YD"]);
                double Finish = Convert.ToDouble(Request["Finish"]);
                double Brash = Convert.ToDouble(Request["Brash"]);
                double Fabriccost = Convert.ToDouble(Request["Fabriccost"]);
                double ConDoz = Convert.ToDouble(Request["ConDoz"]);
                double ConPc = Convert.ToDouble(Request["ConPc"]);
                double ValPc = Convert.ToDouble(Request["ValPc"]);
                double ValDoz = Convert.ToDouble(Request["ValDoz"]);

                List<FabricCost> ItemList = new List<FabricCost>();
                ItemList = (List<FabricCost>)Session["ItemList2"];
                if (ItemList.Count() > 0)
                {
                    FabricCost details = new FabricCost();
                    details.Id = ItemList.Count() + 1;
                    details.FabricId = FabricId;
                    details.Code = Code;
                    details.Sequence = Sequence;
                    details.Unit = Unit;
                    details.Yarn = Yarn;
                    details.Knitting = Knitting;
                    details.Dying = Dying;
                    details.Lycra = Lycra;
                    details.AOP = AOP;
                    details.Wash = Wash;
                    details.YD = YD;
                    details.Finish = Finish;
                    details.Brash = Brash;
                    details.Fabriccost = Fabriccost;
                    details.ConDoz = ConDoz;
                    details.ConPc = ConPc;
                    details.ValPc = ValPc;
                    details.ValDoz = ValDoz;
                    details.Fabric = db.Fabric.Find(FabricId);
                    ItemList.Add(details);
                    Session["ItemList2"] = ItemList;
                }
                else
                {
                    List<FabricCost> ItemLst = new List<FabricCost>();
                    FabricCost details = new FabricCost();
                    details.Id = 1;
                    details.FabricId = FabricId;
                    details.Code = Code;
                    details.Sequence = Sequence;
                    details.Unit = Unit;
                    details.Yarn = Yarn;
                    details.Knitting = Knitting;
                    details.Dying = Dying;
                    details.Lycra = Lycra;
                    details.AOP = AOP;
                    details.Wash = Wash;
                    details.YD = YD;
                    details.Finish = Finish;
                    details.Brash = Brash;
                    details.Fabriccost = Fabriccost;
                    details.ConDoz = ConDoz;
                    details.ConPc = ConPc;
                    details.ValPc = ValPc;
                    details.ValDoz = ValDoz;
                    details.Fabric = db.Fabric.Find(FabricId);
                    ItemLst.Add(details);
                    Session["ItemList2"] = ItemLst;
                }
            }
            return Json(Session["ItemList2"], JsonRequestBehavior.AllowGet);
        }

        [AllowAnonymous]
        [HttpPost]
        public ActionResult DeleteFabricFromList()
        {
            int DetailsId = Convert.ToInt32(Request["DetailsId"]);
            List<FabricCost> ItemLst = new List<FabricCost>();
            ItemLst = (List<FabricCost>)Session["ItemList2"];
            for (int i = 0; i < ItemLst.Count; i++)
            {
                if (ItemLst[i].Id == DetailsId)
                {
                    ItemLst.Remove(ItemLst[i]);
                }
            }
            Session["ItemList2"] = null;

            if (ItemLst.Count > 0)
            {
                Session["ItemList2"] = ItemLst;
            }
            else
            {
                Session["ItemList2"] = new List<FabricCost>();
            }

            return Json(Session["ItemList2"], JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region GetSelectedStepCost
        [AllowAnonymous]
        [HttpPost]
        public ActionResult GetSelectedStepCost()
        {
            if (Request["StepName"].ToString() != "")
            {
                string StepName = Request["StepName"];
                string Remarks = Request["Remarks"];
                double CostPc = Convert.ToDouble(Request["CostPc"]);
                double CostDoz = Convert.ToDouble(Request["CostDoz"]);

                List<StepCost> ItemList = new List<StepCost>();
                ItemList = (List<StepCost>)Session["ItemList3"];
                if (ItemList.Count() > 0)
                {
                    StepCost details = new StepCost();
                    details.Id = ItemList.Count() + 1;
                    details.StepName = StepName;
                    details.Remarks = Remarks;
                    details.CostPc = CostPc;
                    details.CostDoz = CostDoz;
                    ItemList.Add(details);
                    Session["ItemList3"] = ItemList;
                }
                else
                {
                    List<StepCost> ItemLst = new List<StepCost>();
                    StepCost details = new StepCost();
                    details.Id = 1;
                    details.StepName = StepName;
                    details.Remarks = Remarks;
                    details.CostPc = CostPc;
                    details.CostDoz = CostDoz;
                    ItemLst.Add(details);
                    Session["ItemList3"] = ItemLst;
                }
            }
            return Json(Session["ItemList3"], JsonRequestBehavior.AllowGet);
        }

        [AllowAnonymous]
        [HttpPost]
        public ActionResult DeleteStepFromList()
        {
            int DetailsId = Convert.ToInt32(Request["DetailsId"]);
            List<StepCost> ItemLst = new List<StepCost>();
            ItemLst = (List<StepCost>)Session["ItemList3"];
            for (int i = 0; i < ItemLst.Count; i++)
            {
                if (ItemLst[i].Id == DetailsId)
                {
                    ItemLst.Remove(ItemLst[i]);
                }
            }
            Session["ItemList3"] = null;

            if (ItemLst.Count > 0)
            {
                Session["ItemList3"] = ItemLst;
            }
            else
            {
                Session["ItemList3"] = new List<StepCost>();
            }

            return Json(Session["ItemList3"], JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region GetTotalCCost
        [AllowAnonymous]
        [HttpPost]
        public ActionResult GetTotal()
        {
            List<Accessories> ItemList1 = new List<Accessories>();
            ItemList1 = (List<Accessories>)Session["ItemList1"];

            List<FabricCost> ItemList2 = new List<FabricCost>();
            ItemList2 = (List<FabricCost>)Session["ItemList2"];

            List<StepCost> ItemList3 = new List<StepCost>();
            ItemList3 = (List<StepCost>)Session["ItemList3"];

            Total total = new Total();
            if (ItemList1.Count() > 0)
            {
                total.TotalAccesoriesCost = ItemList1.Sum(i => i.TotalCost);
            }
            else
            {
                total.TotalAccesoriesCost = 0;
            }

            if (ItemList2.Count() > 0)
            {
                total.TotalFabricCost = ItemList2.Sum(i => i.ValDoz);
            }
            else
            {
                total.TotalFabricCost = 0;
            }
            if (ItemList3.Count() > 0)
            {
                total.OtherCost = ItemList3.Sum(i => i.CostDoz);
            }
            else
            {
                total.OtherCost = 0;
            }

            double TotalDoz = total.TotalAccesoriesCost + total.TotalFabricCost + total.OtherCost;
            if(TotalDoz != 0)
            {
                total.FOBCost = TotalDoz / 12;
            }

            Session["Total"] = total;
            return Json(Session["Total"], JsonRequestBehavior.AllowGet);
        }
        #endregion

        //GET: PreCostSheets/Create
        public ActionResult Create()
        {
            ViewBag.MarchendiserId = new SelectList(db.Marchendiser, "Id", "Name");
            ViewBag.StyleId = new SelectList(db.Style, "Id", "Knit");
            Session["ItemList1"] = new List<Accessories>();
            Session["ItemList2"] = new List<FabricCost>();
            Session["ItemList3"] = new List<StepCost>();
            return View();
        }

        // POST: PreCostSheets/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,StyleId,FileNumber,CostingDate,BuyerId,Quantity,KnittingTypeId,GSM,ColorRange,SizeRange,StyleDescription,MarchendiserId,FabricDescription,CostCurrency")] PreCostSheet preCostSheet)
        {
            List<Accessories> ItemLst1 = new List<Accessories>();
            ItemLst1 = (List<Accessories>)Session["ItemList1"];
            if (ItemLst1.Count > 0)
            {
                preCostSheet.CostingDate = DateTime.Now.Date;
                db.PreCostSheet.Add(preCostSheet);
                db.SaveChanges();


                #region Add Accesories Details
                List<Accessories> ItemList = new List<Accessories>();
                ItemList = (List<Accessories>)Session["ItemList1"];

                for (int i = 0; i < ItemList.Count; i++)
                {
                    Accessories details = new Accessories();
                    details.ProductId = ItemList[i].ProductId;
                    details.Code = ItemList[i].Code;
                    details.Sequence = ItemList[i].Sequence;
                    details.Unit = ItemList[i].Unit;
                    details.ConDOz = ItemList[i].ConDOz;
                    details.Price = ItemList[i].Price;
                    details.CostDoz = ItemList[i].CostDoz;
                    details.CostPc = ItemList[i].CostPc;
                    details.TotalCost = ItemList[i].TotalCost;
                    details.PreCostId = preCostSheet.Id;
                    db.Accessories.Add(details);
                    db.SaveChanges();
                }
                #endregion

                #region Add Fabric Cost Details
                List<FabricCost> ItemList2 = new List<FabricCost>();
                ItemList2 = (List<FabricCost>)Session["ItemList2"];

                for (int i = 0; i < ItemList2.Count; i++)
                {
                    FabricCost details = new FabricCost();
                    details.FabricId = ItemList2[i].FabricId;
                    details.Code = ItemList2[i].Code;
                    details.Sequence = ItemList2[i].Sequence;
                    details.Unit = ItemList2[i].Unit;
                    details.Yarn = ItemList2[i].Yarn;
                    details.Knitting = ItemList2[i].Knitting;
                    details.Dying = ItemList2[i].Dying;
                    details.Lycra = ItemList2[i].Lycra;
                    details.AOP = ItemList2[i].AOP;
                    details.Wash = ItemList2[i].Wash;
                    details.YD = ItemList2[i].YD;
                    details.Finish = ItemList2[i].Finish;
                    details.Brash = ItemList2[i].Brash;
                    details.Fabriccost = ItemList2[i].Fabriccost;
                    details.ConDoz = ItemList2[i].ConDoz;
                    details.ConPc = ItemList2[i].ConPc;
                    details.ValPc = ItemList2[i].ValPc;
                    details.ValDoz = ItemList2[i].ValDoz;
                    details.PreCostId = preCostSheet.Id;
                    db.FabricCost.Add(details);
                    db.SaveChanges();
                }
                #endregion

                #region Add Step Details
                List<StepCost> ItemList3 = new List<StepCost>();
                ItemList3 = (List<StepCost>)Session["ItemList3"];

                for (int i = 0; i < ItemList3.Count; i++)
                {
                    StepCost details = new StepCost();
                    details.StepName = ItemList3[i].StepName;
                    details.Remarks = ItemList3[i].Remarks;
                    details.CostPc = ItemList3[i].CostPc;
                    details.CostDoz = ItemList3[i].CostDoz;
                    details.PreCostId = preCostSheet.Id;
                    db.StepCost.Add(details);
                    db.SaveChanges();
                }
                #endregion

                #region Add Total Cost Details
                Total total = new Total();
                total = (Total)Session["Total"];
                total.PreCostId = preCostSheet.Id;
                db.Total.Add(total);
                db.SaveChanges();
                #endregion

                return RedirectToAction("Index");
            }

            ViewBag.MarchendiserId = new SelectList(db.Marchendiser, "Id", "Name", preCostSheet.MarchendiserId);
            ViewBag.StyleId = new SelectList(db.Style, "Id", "Knit", preCostSheet.StyleId);
            return View(preCostSheet);
        }

        // GET: PreCostSheets/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PreCostSheet preCostSheet = db.PreCostSheet.Find(id);
            if (preCostSheet == null)
            {
                return HttpNotFound();
            }
            ViewBag.MarchendiserId = new SelectList(db.Marchendiser, "Id", "Name", preCostSheet.MarchendiserId);
            ViewBag.StyleId = new SelectList(db.Style, "Id", "Knit", preCostSheet.StyleId);
            return View(preCostSheet);
        }

        // POST: PreCostSheets/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,StyleId,FileNumber,CostingDate,BuyerId,Quantity,KnittingTypeId,GSM,ColorRange,SizeRange,StyleDescription,MarchendiserId,FabricDescription,CostCurrency")] PreCostSheet preCostSheet)
        {
            if (ModelState.IsValid)
            {
                db.Entry(preCostSheet).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.MarchendiserId = new SelectList(db.Marchendiser, "Id", "Name", preCostSheet.MarchendiserId);
            ViewBag.StyleId = new SelectList(db.Style, "Id", "Knit", preCostSheet.StyleId);
            return View(preCostSheet);
        }

        // GET: PreCostSheets/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PreCostSheet preCostSheet = db.PreCostSheet.Find(id);
            if (preCostSheet == null)
            {
                return HttpNotFound();
            }
            return View(preCostSheet);
        }

        // POST: PreCostSheets/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            PreCostSheet preCostSheet = db.PreCostSheet.Find(id);
            db.PreCostSheet.Remove(preCostSheet);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        #region Print
        public ActionResult PreCostSheetByDateRange()
        {
            List<PreCostSheet> PreCostSheetList = new List<PreCostSheet>();
            return View(PreCostSheetList);
        }
        public ActionResult PreCostSheetByDateRangeView()
        {
            DateTime FromDate = Convert.ToDateTime(Request["FromDate"]);
            DateTime ToDate = Convert.ToDateTime(Request["ToDate"]);
            List<PreCostSheet> PreCostSheetList = db.PreCostSheet.Where(t => t.CostingDate >= FromDate && t.CostingDate <= ToDate).ToList();
            List<Accessories> AccessoriesList = new List<Accessories>();
            List<FabricCost> FabricCostList = new List<FabricCost>();
            List<StepCost> StepCostList = new List<StepCost>();
            List<Total> TotalList = new List<Total>();

            if (FromDate != null && PreCostSheetList.Count() > 0)
            {
                foreach (var item in PreCostSheetList)
                {
                    List<Accessories> accessoriesList = db.Accessories.Where(i => i.PreCostId == item.Id).ToList();
                    foreach (var itm in accessoriesList)
                    {
                        Accessories accessories = db.Accessories.Find(itm.Id);
                        AccessoriesList.Add(accessories);
                    }
                    List<FabricCost> fabricCostList = db.FabricCost.Where(i => i.PreCostId == item.Id).ToList();
                    foreach (var itm in fabricCostList)
                    {
                        FabricCost fabricCost = db.FabricCost.Find(itm.Id);
                        FabricCostList.Add(fabricCost);
                    }
                    List<StepCost> stepCostList = db.StepCost.Where(i => i.PreCostId == item.Id).ToList();
                    foreach (var itm in stepCostList)
                    {
                        StepCost stepCost = db.StepCost.Find(itm.Id);
                        StepCostList.Add(stepCost);
                    }
                    List<Total> totalList = db.Total.Where(i => i.PreCostId == item.Id).ToList();
                    foreach (var itm in totalList)
                    {
                        Total total = db.Total.Find(itm.Id);
                        TotalList.Add(total);
                    }
                }
            }
            Session["PreCostSheetList"] = PreCostSheetList;
            Session["AccessoriesList"] = AccessoriesList;
            Session["FabricCostList"] = FabricCostList;
            Session["StepCostList"] = StepCostList;
            Session["TotalList"] = TotalList;
            return View("OrderRecapByDateRange", PreCostSheetList);
        }

        public ActionResult PrintPreCostSheetByDateRange()
        {
            List<PreCostSheet> PreCostSheetList = new List<PreCostSheet>();
            PreCostSheetList = (List<PreCostSheet>)Session["PreCostSheetList"];
            Session["PreCostSheetList"] = null;
            return View(PreCostSheetList);
        }
        #endregion

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
