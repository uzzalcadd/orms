﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ORMS.Models;

namespace ORMS.Controllers
{
    public class BuyersController : Controller
    {
        private ORMSDbContext db = new ORMSDbContext();

        #region Get Buyers Information
        public ActionResult GetExistingBuyers()
        {
            string BuyerName = Request["BuyerName"];
            List<Buyer> BuyerList = new List<Buyer>();
            if (String.IsNullOrWhiteSpace(BuyerName))
            {
                BuyerList = db.Buyer.Include(i => i.ContactPerson).ToList();
            }
            else
            {
                BuyerList = db.Buyer.Include(i => i.ContactPerson).Where(i => i.Name.Contains(BuyerName)).ToList();
            }
            
            return Json(BuyerList, JsonRequestBehavior.AllowGet);
        }
        #endregion

        // GET: Buyers
        public ActionResult Index()
        {
            var buyer = db.Buyer.Include(b => b.ContactPerson);
            return View(buyer.ToList());
        }

        // GET: Buyers/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Buyer buyer = db.Buyer.Find(id);
            if (buyer == null)
            {
                return HttpNotFound();
            }
            return View(buyer);
        }

        // GET: Buyers/Create
        public ActionResult Create()
        {
            ViewBag.ContactPersonId = new SelectList(db.ContactPerson, "Id", "Name");
            return View();
        }

        // POST: Buyers/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name,Address,Email,ShotName,ContactPersonId")] Buyer buyer)
        {
            if (ModelState.IsValid)
            {
                db.Buyer.Add(buyer);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ContactPersonId = new SelectList(db.ContactPerson, "Id", "Name", buyer.ContactPersonId);
            return View(buyer);
        }

        // GET: Buyers/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Buyer buyer = db.Buyer.Find(id);
            if (buyer == null)
            {
                return HttpNotFound();
            }
            ViewBag.ContactPersonId = new SelectList(db.ContactPerson, "Id", "Name", buyer.ContactPersonId);
            return View(buyer);
        }

        // POST: Buyers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name,Address,Email,ShotName,ContactPersonId")] Buyer buyer)
        {
            if (ModelState.IsValid)
            {
                db.Entry(buyer).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ContactPersonId = new SelectList(db.ContactPerson, "Id", "Name", buyer.ContactPersonId);
            return View(buyer);
        }

        // GET: Buyers/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Buyer buyer = db.Buyer.Find(id);
            if (buyer == null)
            {
                return HttpNotFound();
            }
            return View(buyer);
        }

        // POST: Buyers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Buyer buyer = db.Buyer.Find(id);
            db.Buyer.Remove(buyer);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
