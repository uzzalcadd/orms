﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ORMS.Models;

namespace ORMS.Controllers
{
    public class SizeBreakdownsController : Controller
    {
        private ORMSDbContext db = new ORMSDbContext();

        // GET: SizeBreakdowns
        public ActionResult Index(int id)
        {
            var sizeBreakdown = db.SizeBreakdown.Include(s => s.ColorRange).Include(s => s.OrderRecap).Include(s => s.SizeRange).Where(i => i.OrderRecapId == id);
            ViewBag.OrederRecapId = id;
            return View(sizeBreakdown.ToList());
        }

        // GET: SizeBreakdowns/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SizeBreakdown sizeBreakdown = db.SizeBreakdown.Find(id);
            if (sizeBreakdown == null)
            {
                return HttpNotFound();
            }
            return View(sizeBreakdown);
        }

        // GET: SizeBreakdowns/Create
        public ActionResult Create()
        {
            ViewBag.ColorRangeId = new SelectList(db.ColorRange, "Id", "Name");
            ViewBag.OrderRecapId = new SelectList(db.OrderRecap, "Id", "Session");
            ViewBag.SizeRangeId = new SelectList(db.SizeRange, "Id", "Name");
            return View();
        }

        // POST: SizeBreakdowns/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,ColorRangeId,SizeRangeId,UnitPrice,OrderRecapId")] SizeBreakdown sizeBreakdown)
        {
            if (ModelState.IsValid)
            {
                db.SizeBreakdown.Add(sizeBreakdown);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ColorRangeId = new SelectList(db.ColorRange, "Id", "Name", sizeBreakdown.ColorRangeId);
            ViewBag.OrderRecapId = new SelectList(db.OrderRecap, "Id", "Session", sizeBreakdown.OrderRecapId);
            ViewBag.SizeRangeId = new SelectList(db.SizeRange, "Id", "Name", sizeBreakdown.SizeRangeId);
            return View(sizeBreakdown);
        }

        // GET: SizeBreakdowns/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SizeBreakdown sizeBreakdown = db.SizeBreakdown.Find(id);
            if (sizeBreakdown == null)
            {
                return HttpNotFound();
            }
            ViewBag.ColorRangeId = new SelectList(db.ColorRange, "Id", "Name", sizeBreakdown.ColorRangeId);
            ViewBag.OrderRecapId = new SelectList(db.OrderRecap, "Id", "Session", sizeBreakdown.OrderRecapId);
            ViewBag.SizeRangeId = new SelectList(db.SizeRange, "Id", "Name", sizeBreakdown.SizeRangeId);
            return View(sizeBreakdown);
        }

        // POST: SizeBreakdowns/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,ColorRangeId,SizeRangeId,UnitPrice,OrderRecapId")] SizeBreakdown sizeBreakdown)
        {
            if (ModelState.IsValid)
            {
                db.Entry(sizeBreakdown).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ColorRangeId = new SelectList(db.ColorRange, "Id", "Name", sizeBreakdown.ColorRangeId);
            ViewBag.OrderRecapId = new SelectList(db.OrderRecap, "Id", "Session", sizeBreakdown.OrderRecapId);
            ViewBag.SizeRangeId = new SelectList(db.SizeRange, "Id", "Name", sizeBreakdown.SizeRangeId);
            return View(sizeBreakdown);
        }

        // GET: SizeBreakdowns/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SizeBreakdown sizeBreakdown = db.SizeBreakdown.Find(id);
            if (sizeBreakdown == null)
            {
                return HttpNotFound();
            }
            return View(sizeBreakdown);
        }

        // POST: SizeBreakdowns/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            SizeBreakdown sizeBreakdown = db.SizeBreakdown.Find(id);
            db.SizeBreakdown.Remove(sizeBreakdown);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
