﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ORMS.Models;
using System.Web.Script.Serialization;
using Newtonsoft.Json;

namespace ORMS.Controllers
{
    public class StylesController : Controller
    {
        private ORMSDbContext db = new ORMSDbContext();

        #region Get Styles Information
        public ActionResult GetExistingStyles()
        {
            string StyleNo = Request["StyleNo"];
            List<Style> StyleList = new List<Style>();
            if (String.IsNullOrWhiteSpace(StyleNo))
            {
                StyleList = db.Style.Include(s => s.Brand).Include(s => s.Buyer).Include(s => s.ContactPerson).Include(s => s.Department).Include(s => s.Fabric).Include(s => s.KnittingType).Include(s => s.Marchendiser).ToList();
            }
            else
            {
                StyleList = db.Style.Include(s => s.Brand).Include(s => s.Buyer).Include(s => s.ContactPerson).Include(s => s.Department).Include(s => s.Fabric).Include(s => s.KnittingType).Include(s => s.Marchendiser).Where(i => i.StyleNo.Contains(StyleNo)).ToList();
            }
            return new JsonResult { Data = StyleList, MaxJsonLength = Int32.MaxValue };
        }
        #endregion

        #region List
        // GET: Styles
        public ActionResult Index()
        {
            var style = db.Style.Include(s => s.Brand).Include(s => s.Buyer).Include(s => s.ContactPerson).Include(s => s.Department).Include(s => s.Fabric).Include(s => s.KnittingType).Include(s => s.Marchendiser);
            return View(style.ToList());
        }
        #endregion

        #region Details
        // GET: Styles/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Style style = db.Style.Find(id);
            if (style == null)
            {
                return HttpNotFound();
            }
            return View(style);
        }
        #endregion

        #region Create
        // GET: Styles/Create
        public ActionResult Create()
        {
            ViewBag.BrandId = new SelectList(db.Brand, "Id", "Name");
            ViewBag.BuyerId = new SelectList(db.Buyer, "Id", "Name");
            ViewBag.ContactPersonId = new SelectList(db.ContactPerson, "Id", "Name");
            ViewBag.DepartmentId = new SelectList(db.Department, "Id", "Name");
            ViewBag.FabricId = new SelectList(db.Fabric, "Id", "Name");
            ViewBag.KnittingTypeId = new SelectList(db.KnittingType, "Id", "Name");
            ViewBag.MerchandiserId = new SelectList(db.Marchendiser, "Id", "Name");
            return View();
        }

        // POST: Styles/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,StyleNo,Knit,Session,BuyerId,ContactPersonId,BrandId,DepartmentId,KnittingTypeId,GSM,FabricId,Count,ColorRange,SizeRange,Fabrication,StyleDescription,MerchandiserId,DesignSketch")] Style style, HttpPostedFileBase image1)
        {
            if (image1 != null)
            {
                style.DesignSketch = new byte[image1.ContentLength];
                image1.InputStream.Read(style.DesignSketch, 0, image1.ContentLength);
            }
            else
            {
                style.DesignSketch = new byte[0];
            }

            if (ModelState.IsValid)
            {
                db.Style.Add(style);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.BrandId = new SelectList(db.Brand, "Id", "Name", style.BrandId);
            ViewBag.BuyerId = new SelectList(db.Buyer, "Id", "Name", style.BuyerId);
            ViewBag.ContactPersonId = new SelectList(db.ContactPerson, "Id", "Name", style.ContactPersonId);
            ViewBag.DepartmentId = new SelectList(db.Department, "Id", "Name", style.DepartmentId);
            ViewBag.FabricId = new SelectList(db.Fabric, "Id", "Name", style.FabricId);
            ViewBag.KnittingTypeId = new SelectList(db.KnittingType, "Id", "Name", style.KnittingTypeId);
            ViewBag.MerchandiserId = new SelectList(db.Marchendiser, "Id", "Name", style.MerchandiserId);
            return View(style);
        }
        #endregion

        #region Edit
        // GET: Styles/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Style style = db.Style.Find(id);
            if (style == null)
            {
                return HttpNotFound();
            }
            ViewBag.BrandId = new SelectList(db.Brand, "Id", "Name", style.BrandId);
            ViewBag.BuyerId = new SelectList(db.Buyer, "Id", "Name", style.BuyerId);
            ViewBag.ContactPersonId = new SelectList(db.ContactPerson, "Id", "Name", style.ContactPersonId);
            ViewBag.DepartmentId = new SelectList(db.Department, "Id", "Name", style.DepartmentId);
            ViewBag.FabricId = new SelectList(db.Fabric, "Id", "Name", style.FabricId);
            ViewBag.KnittingTypeId = new SelectList(db.KnittingType, "Id", "Name", style.KnittingTypeId);
            ViewBag.MerchandiserId = new SelectList(db.Marchendiser, "Id", "Name", style.MerchandiserId);
            return View(style);
        }

        // POST: Styles/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,StyleNo,Knit,Session,BuyerId,ContactPersonId,BrandId,DepartmentId,KnittingTypeId,GSM,FabricId,Count,ColorRange,SizeRange,Fabrication,StyleDescription,MerchandiserId,DesignSketch")] Style style, HttpPostedFileBase image1)
        {
            if (image1 != null)
            {
                style.DesignSketch = new byte[image1.ContentLength];
                image1.InputStream.Read(style.DesignSketch, 0, image1.ContentLength);
            }
            else
            {
                style.DesignSketch = db.Style.Where(i => i.Id == style.Id).Select(i => i.DesignSketch).FirstOrDefault();
            }

            if (ModelState.IsValid)
            {
                db.Entry(style).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.BrandId = new SelectList(db.Brand, "Id", "Name", style.BrandId);
            ViewBag.BuyerId = new SelectList(db.Buyer, "Id", "Name", style.BuyerId);
            ViewBag.ContactPersonId = new SelectList(db.ContactPerson, "Id", "Name", style.ContactPersonId);
            ViewBag.DepartmentId = new SelectList(db.Department, "Id", "Name", style.DepartmentId);
            ViewBag.FabricId = new SelectList(db.Fabric, "Id", "Name", style.FabricId);
            ViewBag.KnittingTypeId = new SelectList(db.KnittingType, "Id", "Name", style.KnittingTypeId);
            ViewBag.MerchandiserId = new SelectList(db.Marchendiser, "Id", "Name", style.MerchandiserId);
            return View(style);
        }
        #endregion

        #region Print
        public ActionResult StyleBbyNumber()
        {
            List<Style> StyleList = new List<Style>();
            return View(StyleList);
        }
        public ActionResult StyleBbyNumberView()
        {
            string StyleNo = Request["FromDate"];
            List<Style> StyleList = db.Style.Where(i => i.StyleNo == StyleNo).ToList();
            Session["StyleList"] = StyleList;
            return View("StyleBbyNumber", StyleList);
        }

        public ActionResult PrintStyleByNumber()
        {
            List<Style> StyleList = new List<Style>();
            StyleList = (List<Style>)Session["StyleList"];
            Session["OrderRecapList"] = null;
            return View(StyleList);
        }
        #endregion

        #region Delete
        // GET: Styles/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Style style = db.Style.Find(id);
            if (style == null)
            {
                return HttpNotFound();
            }
            return View(style);
        }

        // POST: Styles/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Style style = db.Style.Find(id);
            db.Style.Remove(style);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
        #endregion

        #region Dispose

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
        #endregion
    }
}
