﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ORMS.Models;

namespace ORMS.Controllers
{
    public class ProducttionFactoriesController : Controller
    {
        private ORMSDbContext db = new ORMSDbContext();

        // GET: ProducttionFactories
        public ActionResult Index()
        {
            return View(db.ProducttionFactory.ToList());
        }

        // GET: ProducttionFactories/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProducttionFactory producttionFactory = db.ProducttionFactory.Find(id);
            if (producttionFactory == null)
            {
                return HttpNotFound();
            }
            return View(producttionFactory);
        }

        // GET: ProducttionFactories/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: ProducttionFactories/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name,Address,Email,ShortName")] ProducttionFactory producttionFactory)
        {
            if (ModelState.IsValid)
            {
                db.ProducttionFactory.Add(producttionFactory);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(producttionFactory);
        }

        // GET: ProducttionFactories/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProducttionFactory producttionFactory = db.ProducttionFactory.Find(id);
            if (producttionFactory == null)
            {
                return HttpNotFound();
            }
            return View(producttionFactory);
        }

        // POST: ProducttionFactories/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name,Address,Email,ShortName")] ProducttionFactory producttionFactory)
        {
            if (ModelState.IsValid)
            {
                db.Entry(producttionFactory).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(producttionFactory);
        }

        // GET: ProducttionFactories/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProducttionFactory producttionFactory = db.ProducttionFactory.Find(id);
            if (producttionFactory == null)
            {
                return HttpNotFound();
            }
            return View(producttionFactory);
        }

        // POST: ProducttionFactories/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ProducttionFactory producttionFactory = db.ProducttionFactory.Find(id);
            db.ProducttionFactory.Remove(producttionFactory);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
