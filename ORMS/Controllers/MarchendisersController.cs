﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ORMS.Models;

namespace ORMS.Controllers
{
    public class MarchendisersController : Controller
    {
        private ORMSDbContext db = new ORMSDbContext();

        #region Get Marchendisers Information
        public ActionResult GetExistingMarchendisers()
        {
            string MarchendiserName = Request["MarchendiserName"];
            List<Marchendiser> MarchendiserList = new List<Marchendiser>();
            if (String.IsNullOrWhiteSpace(MarchendiserName))
            {
                MarchendiserList = db.Marchendiser.ToList();
            }
            else
            {
                MarchendiserList = db.Marchendiser.Where(i => i.Name.Contains(MarchendiserName)).ToList();
            }

            return Json(MarchendiserList, JsonRequestBehavior.AllowGet);
        }
        #endregion

        // GET: Marchendisers
        public ActionResult Index()
        {
            return View(db.Marchendiser.ToList());
        }

        // GET: Marchendisers/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Marchendiser marchendiser = db.Marchendiser.Find(id);
            if (marchendiser == null)
            {
                return HttpNotFound();
            }
            return View(marchendiser);
        }

        // GET: Marchendisers/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Marchendisers/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name,Address,Mobile,Email")] Marchendiser marchendiser)
        {
            if (ModelState.IsValid)
            {
                db.Marchendiser.Add(marchendiser);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(marchendiser);
        }

        // GET: Marchendisers/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Marchendiser marchendiser = db.Marchendiser.Find(id);
            if (marchendiser == null)
            {
                return HttpNotFound();
            }
            return View(marchendiser);
        }

        // POST: Marchendisers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name,Address,Mobile,Email")] Marchendiser marchendiser)
        {
            if (ModelState.IsValid)
            {
                db.Entry(marchendiser).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(marchendiser);
        }

        // GET: Marchendisers/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Marchendiser marchendiser = db.Marchendiser.Find(id);
            if (marchendiser == null)
            {
                return HttpNotFound();
            }
            return View(marchendiser);
        }

        // POST: Marchendisers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Marchendiser marchendiser = db.Marchendiser.Find(id);
            db.Marchendiser.Remove(marchendiser);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
