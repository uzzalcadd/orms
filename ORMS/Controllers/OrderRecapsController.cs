﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ORMS.Models;

namespace ORMS.Controllers
{
    public class OrderRecapsController : Controller
    {
        private ORMSDbContext db = new ORMSDbContext();

        // GET: OrderRecaps
        public ActionResult Index()
        {
            var orderRecap = db.OrderRecap.Include(o => o.Buyer).Include(o => o.ContactPerson).Include(o => o.Marchendiser).Include(o => o.Product).Include(o => o.ProductionFactory).Include(o => o.Style);
            return View(orderRecap.ToList());
        }

        #region Get Color Range Information
        public ActionResult GetExistingColors()
        {
            int StyleId = Convert.ToInt32(Request["StyleId"]);
            string ColorRanges = db.Style.Where(i => i.Id == StyleId).Select(i => i.ColorRange).FirstOrDefault();
            string[] strArray = { };
            if (ColorRanges != null)
            {
                strArray = ColorRanges.Split(',');
            }
            List<ColorRange> ColorRangeList = new List<ColorRange>();
            if (strArray.Length > 0)
            {
                foreach(var item in strArray)
                {
                    ColorRange color = db.ColorRange.Where(i => i.Name == item).FirstOrDefault();
                    ColorRangeList.Add(color);
                }
            }
            return Json(ColorRangeList, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Get Size Range Information
        public ActionResult GetExistingSizes()
        {
            int StyleId = Convert.ToInt32(Request["StyleId"]);
            string SizeRanges = db.Style.Where(i => i.Id == StyleId).Select(i => i.SizeRange).FirstOrDefault();
            string[] strArray = { };
            if (SizeRanges != null)
            {
                strArray = SizeRanges.Split(',');
            }
            List<SizeRange> SizeRangeList = new List<SizeRange>();
            if (strArray.Length > 0)
            {
                foreach (var item in strArray)
                {
                    SizeRange size = db.SizeRange.Where(i => i.Name == item).FirstOrDefault();
                    SizeRangeList.Add(size);
                }
            }
            return Json(SizeRangeList, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region GetSelectedItems
        [AllowAnonymous]
        [HttpPost]
        public ActionResult GetSelectedItems()
        {
            if (Request["StyleId"].ToString() != "")
            {
                int ColorRangeId = Convert.ToInt32(Request["ColorRange"]);
                int SizeRangeId = Convert.ToInt32(Request["SizeRange"]);
                double UnitPrice = Convert.ToDouble(Request["UnitPrice"]);
                double Quantity = Convert.ToDouble(Request["Quantity"]);
                double TotalPrice = Convert.ToDouble(Request["TotalCost"]);

                List<SizeBreakdown> BreakdownList = new List<SizeBreakdown>();
                BreakdownList = (List<SizeBreakdown>)Session["BreakdownList"];
                if (BreakdownList.Count() > 0)
                {
                    SizeBreakdown details = new SizeBreakdown();
                    details.Id = BreakdownList.Count() + 1;
                    details.ColorRangeId = ColorRangeId;
                    details.SizeRangeId = SizeRangeId;
                    details.UnitPrice = UnitPrice;
                    details.Quantity = Quantity;
                    details.TotalCost = TotalPrice;
                    details.ColorRange = db.ColorRange.Find(ColorRangeId);
                    details.SizeRange = db.SizeRange.Find(SizeRangeId);
                    BreakdownList.Add(details);
                    Session["BreakdownList"] = BreakdownList;
                }
                else
                {
                    SizeBreakdown details = new SizeBreakdown();
                    details.Id = 1;
                    details.ColorRangeId = ColorRangeId;
                    details.SizeRangeId = SizeRangeId;
                    details.UnitPrice = UnitPrice;
                    details.Quantity = Quantity;
                    details.TotalCost = TotalPrice;
                    details.ColorRange = db.ColorRange.Find(ColorRangeId);
                    details.SizeRange = db.SizeRange.Find(SizeRangeId);
                    BreakdownList.Add(details);
                    Session["BreakdownList"] = BreakdownList;
                }
            }
            return Json(Session["BreakdownList"], JsonRequestBehavior.AllowGet);
        }

        [AllowAnonymous]
        [HttpPost]
        public ActionResult DeleteFromList()
        {
            int DetailsId = Convert.ToInt32(Request["DetailsId"]);
            List<SizeBreakdown> BreakdownList = new List<SizeBreakdown>();
            BreakdownList = (List<SizeBreakdown>)Session["BreakdownList"];
            for (int i = 0; i < BreakdownList.Count; i++)
            {
                if (BreakdownList[i].Id == DetailsId)
                {
                    BreakdownList.Remove(BreakdownList[i]);
                }
            }
            Session["BreakdownList"] = null;

            if (BreakdownList.Count > 0)
            {
                Session["BreakdownList"] = BreakdownList;
            }
            else
            {
                Session["BreakdownList"] = new List<SizeBreakdown>();
            }

            return Json(Session["BreakdownList"], JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Get Styles Information
        public ActionResult GetExistingStyles()
        {
            string Style = Request["Style"];
            List<Style> StyleList = new List<Style>();
            if (String.IsNullOrWhiteSpace(Style))
            {
                StyleList = db.Style.Include(i => i.Buyer).ToList();
            }
            else
            {
                StyleList = db.Style.Include(i => i.Buyer).Where(i => i.StyleNo.Contains(Style)).ToList();
            }

            return Json(StyleList, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Get Production Factories Information
        public ActionResult GetExistingProductionFactories()
        {
            string ProductionFactoriesName = Request["ProductionFactoryName"];
            List<ProducttionFactory> FactoryList = new List<ProducttionFactory>();
            if (String.IsNullOrWhiteSpace(ProductionFactoriesName))
            {
                FactoryList = db.ProducttionFactory.ToList();
            }
            else
            {
                FactoryList = db.ProducttionFactory.Where(i => i.Name.Contains(ProductionFactoriesName)).ToList();
            }

            return Json(FactoryList, JsonRequestBehavior.AllowGet);
        }
        #endregion

        // GET: OrderRecaps/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OrderRecap orderRecap = db.OrderRecap.Find(id);
            if (orderRecap == null)
            {
                return HttpNotFound();
            }
            return View(orderRecap);
        }

        // GET: OrderRecaps/Create
        public ActionResult Create()
        {
            ViewBag.BuyerId = new SelectList(db.Buyer, "Id", "Name");
            ViewBag.ContactPersonId = new SelectList(db.ContactPerson, "Id", "Name");
            ViewBag.MarchendiserId = new SelectList(db.Marchendiser, "Id", "Name");
            ViewBag.ProductId = new SelectList(db.Product, "Id", "Name");
            ViewBag.ProductionFactoryId = new SelectList(db.ProducttionFactory, "Id", "Name");
            ViewBag.StyleId = new SelectList(db.Style, "Id", "StyleNo");
            Session["BreakdownList"] = new List<SizeBreakdown>();
            return View();
        }

        // POST: OrderRecaps/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,StyleId,OrderNo,Session,Sl,ProductId,BuyerId,ContactPersonId,Count,GSM,OrderDate,ShipmetDate,FatoryShipmentDate,TransportType,MarchendiserId,Currency,ProductionFactoryId,DeliveryTerm,Incoterm,Fabrcation,Remarks")] OrderRecap orderRecap)
        {
            if (ModelState.IsValid)
            {
                db.OrderRecap.Add(orderRecap);
                db.SaveChanges();

                #region Add Details
                List<SizeBreakdown> BreakdownList = new List<SizeBreakdown>();
                BreakdownList = (List<SizeBreakdown>)Session["BreakdownList"];

                for (int i = 0; i < BreakdownList.Count; i++)
                {
                    SizeBreakdown details = new SizeBreakdown();
                    details.ColorRangeId = BreakdownList[i].ColorRangeId;
                    details.SizeRangeId = BreakdownList[i].SizeRangeId;
                    details.UnitPrice = BreakdownList[i].UnitPrice;
                    details.Quantity = BreakdownList[i].Quantity;
                    details.TotalCost = BreakdownList[i].TotalCost;
                    details.OrderRecapId = orderRecap.Id;
                    db.SizeBreakdown.Add(details);
                    db.SaveChanges();
                }
                #endregion

                return RedirectToAction("Index");
            }

            ViewBag.BuyerId = new SelectList(db.Buyer, "Id", "Name", orderRecap.BuyerId);
            ViewBag.ContactPersonId = new SelectList(db.ContactPerson, "Id", "Name", orderRecap.ContactPersonId);
            ViewBag.MarchendiserId = new SelectList(db.Marchendiser, "Id", "Name", orderRecap.MarchendiserId);
            ViewBag.ProductId = new SelectList(db.Product, "Id", "Name", orderRecap.ProductId);
            ViewBag.ProductionFactoryId = new SelectList(db.ProducttionFactory, "Id", "Name", orderRecap.ProductionFactoryId);
            ViewBag.StyleId = new SelectList(db.Style, "Id", "StyleNo", orderRecap.StyleId);
            return View(orderRecap);
        }

        // GET: OrderRecaps/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OrderRecap orderRecap = db.OrderRecap.Find(id);
            if (orderRecap == null)
            {
                return HttpNotFound();
            }
            ViewBag.BuyerId = new SelectList(db.Buyer, "Id", "Name", orderRecap.BuyerId);
            ViewBag.ContactPersonId = new SelectList(db.ContactPerson, "Id", "Name", orderRecap.ContactPersonId);
            ViewBag.MarchendiserId = new SelectList(db.Marchendiser, "Id", "Name", orderRecap.MarchendiserId);
            ViewBag.ProductId = new SelectList(db.Product, "Id", "Name", orderRecap.ProductId);
            ViewBag.ProductionFactoryId = new SelectList(db.ProducttionFactory, "Id", "Name", orderRecap.ProductionFactoryId);
            ViewBag.StyleId = new SelectList(db.Style, "Id", "StyleNo", orderRecap.StyleId);
            return View(orderRecap);
        }

        // POST: OrderRecaps/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,StyleId,OrderNo,Session,Sl,ProductId,BuyerId,ContactPersonId,Count,GSM,OrderDate,ShipmetDate,FatoryShipmentDate,TransportType,MarchendiserId,Currency,ProductionFactoryId,DeliveryTerm,Incoterm,Fabrcation,Remarks")] OrderRecap orderRecap)
        {
            if (ModelState.IsValid)
            {
                db.Entry(orderRecap).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.BuyerId = new SelectList(db.Buyer, "Id", "Name", orderRecap.BuyerId);
            ViewBag.ContactPersonId = new SelectList(db.ContactPerson, "Id", "Name", orderRecap.ContactPersonId);
            ViewBag.MarchendiserId = new SelectList(db.Marchendiser, "Id", "Name", orderRecap.MarchendiserId);
            ViewBag.ProductId = new SelectList(db.Product, "Id", "Name", orderRecap.ProductId);
            ViewBag.ProductionFactoryId = new SelectList(db.ProducttionFactory, "Id", "Name", orderRecap.ProductionFactoryId);
            ViewBag.StyleId = new SelectList(db.Style, "Id", "StyleNo", orderRecap.StyleId);
            return View(orderRecap);
        }

        // GET: OrderRecaps/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OrderRecap orderRecap = db.OrderRecap.Find(id);
            if (orderRecap == null)
            {
                return HttpNotFound();
            }
            return View(orderRecap);
        }

        // POST: OrderRecaps/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            OrderRecap orderRecap = db.OrderRecap.Find(id);
            db.OrderRecap.Remove(orderRecap);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        #region Print
        public ActionResult OrderRecapByDateRange()
        {
            List<OrderRecap> OrderRecapList = new List<OrderRecap>();
            return View(OrderRecapList);
        }
        public ActionResult OrderRecapByDateRangeView()
        {
            DateTime FromDate = Convert.ToDateTime(Request["FromDate"]);
            DateTime ToDate = Convert.ToDateTime(Request["ToDate"]);
            List<OrderRecap> OrderRecapList = db.OrderRecap.Where(t => t.OrderDate >= FromDate && t.OrderDate <= ToDate).ToList();
            List<SizeBreakdown> SizeBreakdownList = new List<SizeBreakdown>();
            if (FromDate != null && OrderRecapList.Count() > 0)
            {
                foreach (var item in OrderRecapList)
                {
                    List<SizeBreakdown> detailsList = db.SizeBreakdown.Where(i => i.OrderRecapId == item.Id).ToList();
                    foreach (var itm in detailsList)
                    {
                        SizeBreakdown details = db.SizeBreakdown.Find(itm.Id);
                        SizeBreakdownList.Add(details);
                    }
                }
            }
            Session["OrderRecapList"] = OrderRecapList;
            Session["SizeBreakdownList"] = SizeBreakdownList;
            return View("OrderRecapByDateRange", OrderRecapList);
        }

        public ActionResult PrintOrderRecapByDateRange()
        {
            List<OrderRecap> OrderRecapList = new List<OrderRecap>();
            OrderRecapList = (List<OrderRecap>)Session["OrderRecapList"];
            Session["OrderRecapList"] = null;
            return View(OrderRecapList);
        }
        #endregion

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
