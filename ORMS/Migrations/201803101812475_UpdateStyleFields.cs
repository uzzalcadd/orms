namespace ORMS.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateStyleFields : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Styles", "ColorRangeId", "dbo.ColorRanges");
            DropForeignKey("dbo.Styles", "SizeRangeId", "dbo.SizeRanges");
            DropForeignKey("dbo.PreCostSheets", "ColorRangeId", "dbo.ColorRanges");
            DropIndex("dbo.Styles", new[] { "ColorRangeId" });
            DropIndex("dbo.Styles", new[] { "SizeRangeId" });
            DropIndex("dbo.PreCostSheets", new[] { "ColorRangeId" });
            AddColumn("dbo.Styles", "ColorRange", c => c.String(nullable: false));
            AddColumn("dbo.Styles", "SizeRange", c => c.String(nullable: false));
            AddColumn("dbo.PreCostSheets", "ColorRange", c => c.String(nullable: false));
            AddColumn("dbo.PreCostSheets", "SizeRange", c => c.String(nullable: false));
            DropColumn("dbo.Styles", "ColorRangeId");
            DropColumn("dbo.Styles", "SizeRangeId");
            DropColumn("dbo.PreCostSheets", "ColorRangeId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.PreCostSheets", "ColorRangeId", c => c.Int(nullable: false));
            AddColumn("dbo.Styles", "SizeRangeId", c => c.Int(nullable: false));
            AddColumn("dbo.Styles", "ColorRangeId", c => c.Int(nullable: false));
            DropColumn("dbo.PreCostSheets", "SizeRange");
            DropColumn("dbo.PreCostSheets", "ColorRange");
            DropColumn("dbo.Styles", "SizeRange");
            DropColumn("dbo.Styles", "ColorRange");
            CreateIndex("dbo.PreCostSheets", "ColorRangeId");
            CreateIndex("dbo.Styles", "SizeRangeId");
            CreateIndex("dbo.Styles", "ColorRangeId");
            AddForeignKey("dbo.PreCostSheets", "ColorRangeId", "dbo.ColorRanges", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Styles", "SizeRangeId", "dbo.SizeRanges", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Styles", "ColorRangeId", "dbo.ColorRanges", "Id", cascadeDelete: true);
        }
    }
}
