namespace ORMS.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddingTwoTables : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.Admins", newName: "Users");
            CreateTable(
                "dbo.FabricCosts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Code = c.String(nullable: false, maxLength: 20),
                        Sequence = c.String(nullable: false, maxLength: 100),
                        Unit = c.String(),
                        Yarn = c.Double(nullable: false),
                        Knitting = c.Double(nullable: false),
                        Dying = c.Double(nullable: false),
                        Lycra = c.Double(nullable: false),
                        AOP = c.Double(nullable: false),
                        Wash = c.Double(nullable: false),
                        YD = c.Double(nullable: false),
                        Finish = c.Double(nullable: false),
                        Brash = c.Double(nullable: false),
                        Fabriccost = c.Double(nullable: false),
                        ConDoz = c.Double(nullable: false),
                        ConPc = c.Double(nullable: false),
                        ValPc = c.Double(nullable: false),
                        ValDoz = c.Double(nullable: false),
                        PreCostId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.PreCostSheets", t => t.PreCostId, cascadeDelete: true)
                .Index(t => t.Code, unique: true)
                .Index(t => t.PreCostId);
            
            CreateTable(
                "dbo.StepCosts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        StepName = c.String(),
                        Remarks = c.String(),
                        CostPc = c.Double(nullable: false),
                        CostDoz = c.Double(nullable: false),
                        PreCostId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.PreCostSheets", t => t.PreCostId, cascadeDelete: true)
                .Index(t => t.PreCostId);
            
            AddColumn("dbo.Accessories", "Name", c => c.String());
            AddColumn("dbo.Accessories", "Unit", c => c.String());
            AddColumn("dbo.Accessories", "ConDOz", c => c.Double(nullable: false));
            AddColumn("dbo.Accessories", "Price", c => c.Double(nullable: false));
            AddColumn("dbo.Accessories", "CostDoz", c => c.Double(nullable: false));
            AddColumn("dbo.Accessories", "CostPc", c => c.Double(nullable: false));
            AddColumn("dbo.Accessories", "TotalCost", c => c.Double(nullable: false));
            AddColumn("dbo.Accessories", "PreCostId", c => c.Int(nullable: false));
            AddColumn("dbo.Users", "Address", c => c.String(nullable: false, maxLength: 250));
            AddColumn("dbo.Users", "UserType", c => c.String(nullable: false, maxLength: 30));
            AlterColumn("dbo.Users", "Name", c => c.String(nullable: false, maxLength: 100));
            AlterColumn("dbo.Users", "FathersName", c => c.String(nullable: false, maxLength: 100));
            AlterColumn("dbo.Users", "MothersName", c => c.String(nullable: false, maxLength: 100));
            AlterColumn("dbo.Users", "Gender", c => c.String(nullable: false, maxLength: 100));
            AlterColumn("dbo.Users", "Mobile", c => c.String(nullable: false, maxLength: 50));
            AlterColumn("dbo.Users", "Email", c => c.String(maxLength: 100));
            AlterColumn("dbo.Users", "Password", c => c.String(nullable: false, maxLength: 100));
            CreateIndex("dbo.Accessories", "PreCostId");
            AddForeignKey("dbo.Accessories", "PreCostId", "dbo.PreCostSheets", "Id", cascadeDelete: true);
            DropColumn("dbo.Accessories", "Estimatedcost");
            DropColumn("dbo.Users", "PresentAddress");
            DropColumn("dbo.Users", "PermanentAddress");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Users", "PermanentAddress", c => c.String(nullable: false, maxLength: 250));
            AddColumn("dbo.Users", "PresentAddress", c => c.String(nullable: false, maxLength: 250));
            AddColumn("dbo.Accessories", "Estimatedcost", c => c.Double(nullable: false));
            DropForeignKey("dbo.StepCosts", "PreCostId", "dbo.PreCostSheets");
            DropForeignKey("dbo.FabricCosts", "PreCostId", "dbo.PreCostSheets");
            DropForeignKey("dbo.Accessories", "PreCostId", "dbo.PreCostSheets");
            DropIndex("dbo.StepCosts", new[] { "PreCostId" });
            DropIndex("dbo.FabricCosts", new[] { "PreCostId" });
            DropIndex("dbo.FabricCosts", new[] { "Code" });
            DropIndex("dbo.Accessories", new[] { "PreCostId" });
            AlterColumn("dbo.Users", "Password", c => c.String(nullable: false, maxLength: 250));
            AlterColumn("dbo.Users", "Email", c => c.String(maxLength: 250));
            AlterColumn("dbo.Users", "Mobile", c => c.String(nullable: false, maxLength: 450));
            AlterColumn("dbo.Users", "Gender", c => c.String(nullable: false, maxLength: 250));
            AlterColumn("dbo.Users", "MothersName", c => c.String(nullable: false, maxLength: 250));
            AlterColumn("dbo.Users", "FathersName", c => c.String(nullable: false, maxLength: 250));
            AlterColumn("dbo.Users", "Name", c => c.String(nullable: false, maxLength: 250));
            DropColumn("dbo.Users", "UserType");
            DropColumn("dbo.Users", "Address");
            DropColumn("dbo.Accessories", "PreCostId");
            DropColumn("dbo.Accessories", "TotalCost");
            DropColumn("dbo.Accessories", "CostPc");
            DropColumn("dbo.Accessories", "CostDoz");
            DropColumn("dbo.Accessories", "Price");
            DropColumn("dbo.Accessories", "ConDOz");
            DropColumn("dbo.Accessories", "Unit");
            DropColumn("dbo.Accessories", "Name");
            DropTable("dbo.StepCosts");
            DropTable("dbo.FabricCosts");
            RenameTable(name: "dbo.Users", newName: "Admins");
        }
    }
}
