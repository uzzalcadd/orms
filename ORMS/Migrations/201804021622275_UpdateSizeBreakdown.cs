namespace ORMS.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateSizeBreakdown : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.SizeBreakdowns", name: "ColorId", newName: "ColorRangeId");
            RenameIndex(table: "dbo.SizeBreakdowns", name: "IX_ColorId", newName: "IX_ColorRangeId");
            AddColumn("dbo.SizeBreakdowns", "SizeRangeId", c => c.Int(nullable: false));
            AddColumn("dbo.SizeBreakdowns", "OrderRecapId", c => c.Int(nullable: false));
            CreateIndex("dbo.SizeBreakdowns", "SizeRangeId");
            CreateIndex("dbo.SizeBreakdowns", "OrderRecapId");
            AddForeignKey("dbo.SizeBreakdowns", "OrderRecapId", "dbo.OrderRecaps", "Id", cascadeDelete: false);
            AddForeignKey("dbo.SizeBreakdowns", "SizeRangeId", "dbo.SizeRanges", "Id", cascadeDelete: false);
            DropColumn("dbo.SizeBreakdowns", "S");
            DropColumn("dbo.SizeBreakdowns", "M");
            DropColumn("dbo.SizeBreakdowns", "L");
            DropColumn("dbo.SizeBreakdowns", "XL");
            DropColumn("dbo.SizeBreakdowns", "XS");
        }
        
        public override void Down()
        {
            AddColumn("dbo.SizeBreakdowns", "XS", c => c.Int());
            AddColumn("dbo.SizeBreakdowns", "XL", c => c.Int());
            AddColumn("dbo.SizeBreakdowns", "L", c => c.Int());
            AddColumn("dbo.SizeBreakdowns", "M", c => c.Int());
            AddColumn("dbo.SizeBreakdowns", "S", c => c.Int());
            DropForeignKey("dbo.SizeBreakdowns", "SizeRangeId", "dbo.SizeRanges");
            DropForeignKey("dbo.SizeBreakdowns", "OrderRecapId", "dbo.OrderRecaps");
            DropIndex("dbo.SizeBreakdowns", new[] { "OrderRecapId" });
            DropIndex("dbo.SizeBreakdowns", new[] { "SizeRangeId" });
            DropColumn("dbo.SizeBreakdowns", "OrderRecapId");
            DropColumn("dbo.SizeBreakdowns", "SizeRangeId");
            RenameIndex(table: "dbo.SizeBreakdowns", name: "IX_ColorRangeId", newName: "IX_ColorId");
            RenameColumn(table: "dbo.SizeBreakdowns", name: "ColorRangeId", newName: "ColorId");
        }
    }
}
