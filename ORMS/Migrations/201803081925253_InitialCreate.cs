namespace ORMS.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Accessories",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Code = c.String(nullable: false, maxLength: 20),
                        Sequence = c.String(nullable: false, maxLength: 100),
                        Estimatedcost = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Code, unique: true);
            
            CreateTable(
                "dbo.Admins",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Code = c.String(nullable: false, maxLength: 15),
                        Name = c.String(nullable: false, maxLength: 250),
                        FathersName = c.String(nullable: false, maxLength: 250),
                        MothersName = c.String(nullable: false, maxLength: 250),
                        Gender = c.String(nullable: false, maxLength: 250),
                        PresentAddress = c.String(nullable: false, maxLength: 250),
                        PermanentAddress = c.String(nullable: false, maxLength: 250),
                        Mobile = c.String(nullable: false, maxLength: 450),
                        Email = c.String(maxLength: 250),
                        Password = c.String(nullable: false, maxLength: 250),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Code, unique: true);
            
            CreateTable(
                "dbo.Brands",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 100),
                        Description = c.String(maxLength: 300),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Buyers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 100),
                        Address = c.String(maxLength: 300),
                        Email = c.String(nullable: false, maxLength: 100),
                        ShotName = c.String(maxLength: 50),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ColorRanges",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 100),
                        Note = c.String(maxLength: 200),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ContactPersons",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 50),
                        Address = c.String(maxLength: 300),
                        Phone = c.String(nullable: false, maxLength: 50),
                        Email = c.String(nullable: false, maxLength: 100),
                        Note = c.String(maxLength: 100),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Departments",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 200),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Fabrics",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 100),
                        Description = c.String(maxLength: 200),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.KnittingTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 200),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Marchendisers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 100),
                        Address = c.String(maxLength: 300),
                        Mobile = c.String(nullable: false, maxLength: 50),
                        Email = c.String(nullable: false, maxLength: 100),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.OrderRecaps",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        StyleId = c.Int(nullable: false),
                        OrderNo = c.Int(nullable: false),
                        Session = c.String(),
                        Sl = c.Int(nullable: false),
                        ProductId = c.Int(nullable: false),
                        BuyerId = c.Int(nullable: false),
                        ContactPersonId = c.Int(nullable: false),
                        Count = c.Int(nullable: false),
                        GSM = c.String(nullable: false),
                        OrderDate = c.DateTime(nullable: false),
                        ShipmetDate = c.DateTime(nullable: false),
                        FatoryShipmentDate = c.DateTime(nullable: false),
                        TransportType = c.String(nullable: false),
                        MarchendiserId = c.Int(nullable: false),
                        Currency = c.String(nullable: false, maxLength: 30),
                        ProductionFactoryId = c.Int(nullable: false),
                        DeliveryTerm = c.String(nullable: false, maxLength: 50),
                        Incoterm = c.String(nullable: false, maxLength: 100),
                        Fabrcation = c.String(nullable: false, maxLength: 200),
                        Remarks = c.String(maxLength: 300),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Buyers", t => t.BuyerId, cascadeDelete: true)
                .ForeignKey("dbo.ContactPersons", t => t.ContactPersonId, cascadeDelete: true)
                .ForeignKey("dbo.Marchendisers", t => t.MarchendiserId, cascadeDelete: true)
                .ForeignKey("dbo.Products", t => t.ProductId, cascadeDelete: true)
                .ForeignKey("dbo.ProducttionFactories", t => t.ProductionFactoryId, cascadeDelete: true)
                .ForeignKey("dbo.Styles", t => t.StyleId, cascadeDelete: true)
                .Index(t => t.StyleId)
                .Index(t => t.ProductId)
                .Index(t => t.BuyerId)
                .Index(t => t.ContactPersonId)
                .Index(t => t.MarchendiserId)
                .Index(t => t.ProductionFactoryId);
            
            CreateTable(
                "dbo.Products",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 100),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ProducttionFactories",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 100),
                        Address = c.String(maxLength: 300),
                        Email = c.String(nullable: false, maxLength: 100),
                        ShortName = c.String(maxLength: 50),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Styles",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Knit = c.String(nullable: false),
                        Session = c.String(nullable: false, maxLength: 100),
                        BuyerId = c.Int(nullable: false),
                        ContactPersonId = c.Int(nullable: false),
                        BrandId = c.Int(),
                        DepartmentId = c.Int(nullable: false),
                        KnittingTypeId = c.Int(nullable: false),
                        GSM = c.String(nullable: false, maxLength: 50),
                        FabricId = c.Int(nullable: false),
                        Count = c.Int(nullable: false),
                        ColorRangeId = c.Int(nullable: false),
                        SizeRangeId = c.Int(nullable: false),
                        Fabrication = c.String(nullable: false, maxLength: 200),
                        StyleDescription = c.String(),
                        MerchandiserId = c.Int(nullable: false),
                        DesignSketch = c.Binary(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Brands", t => t.BrandId)
                .ForeignKey("dbo.Buyers", t => t.BuyerId, cascadeDelete: false)
                .ForeignKey("dbo.ColorRanges", t => t.ColorRangeId, cascadeDelete: true)
                .ForeignKey("dbo.ContactPersons", t => t.ContactPersonId, cascadeDelete: false)
                .ForeignKey("dbo.Departments", t => t.DepartmentId, cascadeDelete: true)
                .ForeignKey("dbo.Fabrics", t => t.FabricId, cascadeDelete: true)
                .ForeignKey("dbo.KnittingTypes", t => t.KnittingTypeId, cascadeDelete: true)
                .ForeignKey("dbo.Marchendisers", t => t.MerchandiserId, cascadeDelete: false)
                .ForeignKey("dbo.SizeRanges", t => t.SizeRangeId, cascadeDelete: true)
                .Index(t => t.BuyerId)
                .Index(t => t.ContactPersonId)
                .Index(t => t.BrandId)
                .Index(t => t.DepartmentId)
                .Index(t => t.KnittingTypeId)
                .Index(t => t.FabricId)
                .Index(t => t.ColorRangeId)
                .Index(t => t.SizeRangeId)
                .Index(t => t.MerchandiserId);
            
            CreateTable(
                "dbo.SizeRanges",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 100),
                        Note = c.String(maxLength: 100),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.PreCostSheets",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        StyleId = c.Int(nullable: false),
                        FileNumber = c.Int(nullable: false),
                        CostingDate = c.DateTime(nullable: false),
                        BuyerId = c.Int(nullable: false),
                        Quantity = c.Double(nullable: false),
                        Knitting = c.String(nullable: false),
                        GSM = c.String(nullable: false),
                        ColorRangeId = c.Int(nullable: false),
                        StyleDescription = c.String(maxLength: 300),
                        MarchendiserId = c.Int(nullable: false),
                        FabricDescription = c.String(maxLength: 200),
                        CostCurrency = c.String(nullable: false, maxLength: 50),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Buyers", t => t.BuyerId, cascadeDelete: true)
                .ForeignKey("dbo.ColorRanges", t => t.ColorRangeId, cascadeDelete: true)
                .ForeignKey("dbo.Marchendisers", t => t.MarchendiserId, cascadeDelete: true)
                .ForeignKey("dbo.Styles", t => t.StyleId, cascadeDelete: false)
                .Index(t => t.StyleId)
                .Index(t => t.BuyerId)
                .Index(t => t.ColorRangeId)
                .Index(t => t.MarchendiserId);
            
            CreateTable(
                "dbo.RefTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 100),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.SizeBreakdowns",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ColorId = c.Int(nullable: false),
                        S = c.Int(),
                        M = c.Int(),
                        L = c.Int(),
                        XL = c.Int(),
                        XS = c.Int(),
                        UnitPrice = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ColorRanges", t => t.ColorId, cascadeDelete: true)
                .Index(t => t.ColorId);
            
            CreateTable(
                "dbo.Yarns",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Code = c.String(nullable: false, maxLength: 20),
                        Name = c.String(nullable: false, maxLength: 100),
                        Sequence = c.String(nullable: false, maxLength: 100),
                        Ply = c.String(nullable: false, maxLength: 20),
                        Price = c.Double(nullable: false),
                        Use = c.Double(nullable: false),
                        EstimatedCost = c.Double(nullable: false),
                        Wastage = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Code, unique: true);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.SizeBreakdowns", "ColorId", "dbo.ColorRanges");
            DropForeignKey("dbo.PreCostSheets", "StyleId", "dbo.Styles");
            DropForeignKey("dbo.PreCostSheets", "MarchendiserId", "dbo.Marchendisers");
            DropForeignKey("dbo.PreCostSheets", "ColorRangeId", "dbo.ColorRanges");
            DropForeignKey("dbo.PreCostSheets", "BuyerId", "dbo.Buyers");
            DropForeignKey("dbo.OrderRecaps", "StyleId", "dbo.Styles");
            DropForeignKey("dbo.Styles", "SizeRangeId", "dbo.SizeRanges");
            DropForeignKey("dbo.Styles", "MerchandiserId", "dbo.Marchendisers");
            DropForeignKey("dbo.Styles", "KnittingTypeId", "dbo.KnittingTypes");
            DropForeignKey("dbo.Styles", "FabricId", "dbo.Fabrics");
            DropForeignKey("dbo.Styles", "DepartmentId", "dbo.Departments");
            DropForeignKey("dbo.Styles", "ContactPersonId", "dbo.ContactPersons");
            DropForeignKey("dbo.Styles", "ColorRangeId", "dbo.ColorRanges");
            DropForeignKey("dbo.Styles", "BuyerId", "dbo.Buyers");
            DropForeignKey("dbo.Styles", "BrandId", "dbo.Brands");
            DropForeignKey("dbo.OrderRecaps", "ProductionFactoryId", "dbo.ProducttionFactories");
            DropForeignKey("dbo.OrderRecaps", "ProductId", "dbo.Products");
            DropForeignKey("dbo.OrderRecaps", "MarchendiserId", "dbo.Marchendisers");
            DropForeignKey("dbo.OrderRecaps", "ContactPersonId", "dbo.ContactPersons");
            DropForeignKey("dbo.OrderRecaps", "BuyerId", "dbo.Buyers");
            DropIndex("dbo.Yarns", new[] { "Code" });
            DropIndex("dbo.SizeBreakdowns", new[] { "ColorId" });
            DropIndex("dbo.PreCostSheets", new[] { "MarchendiserId" });
            DropIndex("dbo.PreCostSheets", new[] { "ColorRangeId" });
            DropIndex("dbo.PreCostSheets", new[] { "BuyerId" });
            DropIndex("dbo.PreCostSheets", new[] { "StyleId" });
            DropIndex("dbo.Styles", new[] { "MerchandiserId" });
            DropIndex("dbo.Styles", new[] { "SizeRangeId" });
            DropIndex("dbo.Styles", new[] { "ColorRangeId" });
            DropIndex("dbo.Styles", new[] { "FabricId" });
            DropIndex("dbo.Styles", new[] { "KnittingTypeId" });
            DropIndex("dbo.Styles", new[] { "DepartmentId" });
            DropIndex("dbo.Styles", new[] { "BrandId" });
            DropIndex("dbo.Styles", new[] { "ContactPersonId" });
            DropIndex("dbo.Styles", new[] { "BuyerId" });
            DropIndex("dbo.OrderRecaps", new[] { "ProductionFactoryId" });
            DropIndex("dbo.OrderRecaps", new[] { "MarchendiserId" });
            DropIndex("dbo.OrderRecaps", new[] { "ContactPersonId" });
            DropIndex("dbo.OrderRecaps", new[] { "BuyerId" });
            DropIndex("dbo.OrderRecaps", new[] { "ProductId" });
            DropIndex("dbo.OrderRecaps", new[] { "StyleId" });
            DropIndex("dbo.Admins", new[] { "Code" });
            DropIndex("dbo.Accessories", new[] { "Code" });
            DropTable("dbo.Yarns");
            DropTable("dbo.SizeBreakdowns");
            DropTable("dbo.RefTypes");
            DropTable("dbo.PreCostSheets");
            DropTable("dbo.SizeRanges");
            DropTable("dbo.Styles");
            DropTable("dbo.ProducttionFactories");
            DropTable("dbo.Products");
            DropTable("dbo.OrderRecaps");
            DropTable("dbo.Marchendisers");
            DropTable("dbo.KnittingTypes");
            DropTable("dbo.Fabrics");
            DropTable("dbo.Departments");
            DropTable("dbo.ContactPersons");
            DropTable("dbo.ColorRanges");
            DropTable("dbo.Buyers");
            DropTable("dbo.Brands");
            DropTable("dbo.Admins");
            DropTable("dbo.Accessories");
        }
    }
}
