namespace ORMS.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateInFabriccost : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.FabricCosts", "FabricId", c => c.Int(nullable: false));
            CreateIndex("dbo.FabricCosts", "FabricId");
            AddForeignKey("dbo.FabricCosts", "FabricId", "dbo.Fabrics", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.FabricCosts", "FabricId", "dbo.Fabrics");
            DropIndex("dbo.FabricCosts", new[] { "FabricId" });
            DropColumn("dbo.FabricCosts", "FabricId");
        }
    }
}
