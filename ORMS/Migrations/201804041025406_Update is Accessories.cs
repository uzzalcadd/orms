namespace ORMS.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateisAccessories : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Accessories", "ProductId", c => c.Int(nullable: false));
            CreateIndex("dbo.Accessories", "ProductId");
            AddForeignKey("dbo.Accessories", "ProductId", "dbo.Products", "Id", cascadeDelete: true);
            DropColumn("dbo.Accessories", "Name");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Accessories", "Name", c => c.String());
            DropForeignKey("dbo.Accessories", "ProductId", "dbo.Products");
            DropIndex("dbo.Accessories", new[] { "ProductId" });
            DropColumn("dbo.Accessories", "ProductId");
        }
    }
}
