namespace ORMS.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateInSizeBreakdown : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.SizeBreakdowns", "Quantity", c => c.Double(nullable: false));
            AddColumn("dbo.SizeBreakdowns", "TotalCost", c => c.Double(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.SizeBreakdowns", "TotalCost");
            DropColumn("dbo.SizeBreakdowns", "Quantity");
        }
    }
}
