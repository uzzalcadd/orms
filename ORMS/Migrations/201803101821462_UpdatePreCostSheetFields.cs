namespace ORMS.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdatePreCostSheetFields : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PreCostSheets", "KnittingTypeId", c => c.Int(nullable: false));
            CreateIndex("dbo.PreCostSheets", "KnittingTypeId");
            AddForeignKey("dbo.PreCostSheets", "KnittingTypeId", "dbo.KnittingTypes", "Id", cascadeDelete: false);
            DropColumn("dbo.PreCostSheets", "Knitting");
        }
        
        public override void Down()
        {
            AddColumn("dbo.PreCostSheets", "Knitting", c => c.String(nullable: false));
            DropForeignKey("dbo.PreCostSheets", "KnittingTypeId", "dbo.KnittingTypes");
            DropIndex("dbo.PreCostSheets", new[] { "KnittingTypeId" });
            DropColumn("dbo.PreCostSheets", "KnittingTypeId");
        }
    }
}
