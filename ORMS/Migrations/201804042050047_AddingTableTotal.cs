namespace ORMS.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddingTableTotal : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Totals",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TotalFabricCost = c.Double(nullable: false),
                        TotalAccesoriesCost = c.Double(nullable: false),
                        OtherCost = c.Double(nullable: false),
                        FOBCost = c.Double(nullable: false),
                        PreCostId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.PreCostSheets", t => t.PreCostId, cascadeDelete: true)
                .Index(t => t.PreCostId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Totals", "PreCostId", "dbo.PreCostSheets");
            DropIndex("dbo.Totals", new[] { "PreCostId" });
            DropTable("dbo.Totals");
        }
    }
}
