namespace ORMS.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateBuyer : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Buyers", "ContactPersonId", c => c.Int(nullable: false));
            CreateIndex("dbo.Buyers", "ContactPersonId");
            AddForeignKey("dbo.Buyers", "ContactPersonId", "dbo.ContactPersons", "Id", cascadeDelete: false);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Buyers", "ContactPersonId", "dbo.ContactPersons");
            DropIndex("dbo.Buyers", new[] { "ContactPersonId" });
            DropColumn("dbo.Buyers", "ContactPersonId");
        }
    }
}
