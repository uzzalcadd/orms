namespace ORMS.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddingStyleNo : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Styles", "StyleNo", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Styles", "StyleNo");
        }
    }
}
